# Orbital CLI Project

This project is a utility to assist with the deployment and implementation of the Orbital Bus constructed by Foci Solutions. Using this project, you can generate template files and ultimately actual code libraries necessary for the deployment of an Orbital agent module.

## How to create a service

## Create a Service

Open a terminal where you want to create your service. 


```
orbital create:service ServiceName

```

This will create a directory with the name provided, `ServiceName`, and a `.orbital` file that will look like.

```json
{
    "serviceName": "ServiceName",
    "consul": {
        "address": "localhost",
        "port": 8500
    },
    "rabbit": {
        "address": "localhost",
        "port": 5672,
        "username": "guest",
        "password": "guest"
    }
}
```

If the addresses and/or ports for Consul or RabbitMQ are different from the above, change them.

## Create an Operation

Now is the time to create an operation. For the purpose of this example we will create just one operation.

Go to the `ServiceName` folder and run the following command

```
orbital create:operation OperationName -a Adapter.Rest -s
```

It will create the following folder structure

```

ServiceName
    |___ OperationName
    |          |__adapterConfigurations
    |                     |__adapterConfig.json
    |          |__schemas
    |                |__request.json
    |                |__response.json
    |          |__translations
    |                |__translation.js
    |___.orbital

```

Inside the `OperationName` folder, we will modify the following:

* adapterConfigurations
* schemas
* translations

Starting with `adapterConfigurations`, you will open `adapterConfig.json` and should look like this:


```json
{
    "type": "Adapters.Rest",
    "configuration": 
    {
        "uri": "",
        "method": "GET",
        "headers": [
              {
                "key": "Content-Type",
                "value": "application/json"
              }
          ]
    }

}
```
Here you will write in the `uri` section to indicate the endpoint you want to reach and add or modify other sections to comply with the requisits the endpoint asks to be able to get a response back.

For `schemas`, the `request.json` or `response.json` schema should look like this:

```json
{
    "title": "",
    "description": "",
    "type": "object",
    "typeName": "",
    "properties": {
        "id": {
            "type": "string"
        }
    }
}
```
You will accomodate the schemas to the properties you need to receive or send.

Finally our `translations` looks like:

[!code-javascript[GetUserTranslation](SampleCode/GetUserTranslation.js#L28-L48 "GetUserTranslation")]

You should not modify the name of these functions at all. You can do all the javascript code inside these 3 functions to manipulate the data you send or receive.

## Build the Service

Still inside the `ServiceName` folder, you will write the following command:

```
orbital build
```

This command will create a `bin` folder with a `ServiceDefinition.json` file. This file will be used by the Agent to know about the services you create.


## Publish the Service

Finally you will run the following command to publish your new service to Consul and RabbitMQ:

```
orbital publish
```

you can check out Consul to verify that your `ServiceName` service and `orbital.ServiceName.definition` K/V are there.


## Licensing Information

Orbital CLI is licensed under the 3-Clause BSD license for open-source software. Please see the [LICENSE](LICENSE.md) file for more information.  


## Contact Information
For inquiries about the project, please e-mail us at [opensource@focisolutions.com](mailto:opensource@focisolutions.com).

# Service Configuration Specifications

In Orbital, the service configuration files are used to configure the endpoints used by the Adapter. Currently Orbital supports only its REST Adapter. This document will outline the expected properties and format of the service configurations.

## Example Schema  
The service configuration files are JSON files. To know more about JSON schemas you can take a look at the [open standard](http://json-schema.org/latest/json-schema-core.html) and [space telescope documentation](https://spacetelescope.github.io/understanding-json-schema/). To know more about JSON schemas you can take a look at as follows:

```json
{
  "uri": "http://{{ServiceIp}}:{{ServicePort}}/customer?Id={{customerId}}",
  "method": "GET",
  "headers": [
    {
      "key": "Content-Type",
      "value": "application/json"
    }
  ]
}
```

* The `uri` is the destination for the HTTP call. You will notice in the example there are portions of the URI in `{{}}`. These are parameter values. For more information [see below](#uri-parameters).
* The `method` parameter must be one of the HTTP verbs compatible with a REST call. They include:
  * `GET`
  * `POST`
  * `PUT`
  * `PATCH`
  * `DELETE`
* The `headers` parameter is a list of key/value pairs to be sent in the headers of the HTTP request. If you want to use basic HTTP authentication without the keystore you can enter the credentials as a key/value pair in the headers.

## URI Parameters  
Many RESTful services require parameters to be submitted as part of the URI. Orbital supports these parameters in to ways: via the configuration and via the message. These parameters both function by encapsulating the key for the parameter in double curly braces (`{{parameterKey}}`). Parameters in the URI are optional and can include parameters from either source so long as all parameters have unique keys.  
### Via the Configuration
The Orbital agent in the Orbital has a property in its configuration item called `ServiceStaticParameters`. This property is a list of key-value pairs. The keys must match the URI placeholders. The values will be submitted to replace them. These types of parameters should only be used if their values will remain consistent for all calls through the Orbital agent.
i.e. 
```json
"ServiceStaticParameters": {
      "ServiceIp": "localhost",
      "ServicePort": "3580"
    }
```
in conjunction with the above [example](#example-schema) will result in
```json
"http://localhost:3580/customer?Id={{customerId}}"
```
### Via the Message
It is not uncommon for a URI to require an ID or some other value specific to the message. For this reason, the translations in Orbital can be used to place information from the message directly into the URI. More information about the translation implementation can be found in the [translation specifications](TranslationSpecs.html).
i.e.
```javascript
function getDynamicParameters(message) {
    var messageParts = {
        "customerId": message.Id
    }
    return messageParts;
}
```
in the translation file with the above [example](#example-schema) will result in
```json
"http://localhost:3580/customer?Id=123"
```
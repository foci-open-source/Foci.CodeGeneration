# Translation Specifications

While Orbital won’t prevent anyone from using the same translation file for multiple calls, the recommended course of action is to have a separate file for each operation. Each function takes in the *message* object that is passed in from the Orbital agent. While translations are necessary for any call, your translations can simply pass the *message* parameter back as the response if no mutation or validation is necessary.

Asynchronous calls have two functions:
* `serviceInboundTranslation(message)`: This function is performed on messages before they pass from the Orbital agent to the service. It is called *service inbound* because it is flowing *into* the consumer service. This function should take the *message* object and return an object properly formed for being sent to the service.  
For example, suppose the Orbital agent gets a message that is a `customer` object with three properties: `firstName`, `middleName`, and `lastName`. A translation that would prepare the message for an endpoint that has a single property called `fullName` would look like the following:  
[!code-javascript[ServiceInboundExample](SampleCode/ServiceInboundTranslationSample.js "ServiceInboundExample")]

* `getDynamicParameters(message)`: This function should take in the *message* object and return a key/value list of parameters to inject into the service configuration for the operation. Further information about using service configuration parameters can be found in the [Service Configuration Specifications](ServiceConfigSpecs.html).  
For example, suppose the message passing through the Orbital agent contains a `customerId` property that is required in the URL and the service configuration is properly configured to inject the value in place of the `CID`. The translation would look like the following:  
[!code-javascript[GetDynamicParametersExample](SampleCode/GetDynamicParametersSample.js "GetDynamicParamsExample")]


Synchronous calls have the two functions of asynchronous functions calls, as well as a third:  
* `serviceOutboundTranslation(message)`: This function is performed on messages as they come back into the Orbital agent from the service. It is called *service outbound* because it is flowing *out* from the consumer service. It takes the *message* object returned from the service and should return an object to be deserialized and passed back through the Orbital onto the producer.
For example, if the consumer returned an object with an IP address and a port as separate properties, but the Orbital agent is expecting a single `ipAdress`, the translation would look like the following:  
[!code-javascript[ServiceOutboundExample](SampleCode/ServiceOutboundTranslationSample.js "ServiceOutbondExample")]  


The following optional function calls are also available:
* `throwBusinessError(data, headers)`: Triggers the throwing of a business fault.  The *data* object will be serialized and transmitted as the message of the fault.  The *headers* object is a key/value collection of strings to contain information to better inform you on how to deserialize or manage the information packaged in the data.
* `logInfo(information)`: Writes the *information*, either as a string or as a serialized object, to the logs at the INFO level.
* `logTrace(information)`: Writes the *information*, either as a string or as a serialized object, to the logs at the TRACE level.
* `logDebug(information)`: Writes the *information*, either as a string or as a serialized object, to the logs at the DEBUG level.
* `logWarn(information)`: Writes the *information*, either as a string or as a serialized object, to the logs at the WARN level.
* `logError(information)`: Writes the *information*, either as a string or as a serialized object, to the logs at the ERROR level.
* `logFatal(information)`: Writes the *information*, either as a string or as a serialized object, to the logs at the FATAL level.

Any exception thrown from the translation layer without using the `throwBusinessError` method call will be caught as a runtime exception and will be passed up as such.  Using these exceptions is strongly discouraged since they should only be thrown when the code is malformed and not when information fails validation.

**Note:** All these function calls in the translations are case sensitive.
# Welcome to Orbital CLI!

The Orbital CLI project is a companion for the [Orbital project](http://www.orbitalbus.com).  It is used to generate service definition that works with the Orbital agent for consumer services.

For more information check out our [README](articles/README.html). Also be sure to check out our [repository](https://gitlab.com/foci-open-source/Foci.CodeGeneration/uploads/b3b268eb4f9bafed6c06eabb455ecb77/Foci.Orbital.CodeGeneration.zip).

**Check out our Beta release here: [https://gitlab.com/foci-open-source/Foci.CodeGeneration/tags/v0.2.0/Foci.Orbital.CodeGen.zip](https://gitlab.com/foci-open-source/Foci.CodeGeneration/uploads/d9bc567491439b714a00182304c1056b/Foci.Orbital.CodeGen.zip).**
import { assert } from 'chai';
import { Rabbitmq } from '../../../../src/orbital-cli.domain/aggregates/publish-service-definition.aggregate/rabbitmq-configuration';
import { ConsulConfiguration } from '../../../../src/orbital-cli.domain/aggregates/publish-service-definition.aggregate/consul-configuration';
import { OrbitalConfiguration } from '../../../../src/orbital-cli.domain/aggregates/publish-service-definition.aggregate/orbital-configuration';

describe('orbital configuration', () => {
    const rabbit = {
        address: 'localhost',
        password: 'guest',
        port: '8500',
        username: 'guest'
    };

    const consulconfig = {
        address: 'localhost',
        port: '8500'
    };
    const input = {
        consul: consulconfig,
        rabbitmq: rabbit,
        servicename: 'exampleService'
    };

    const rabbitconfig: Rabbitmq = new Rabbitmq(rabbit.address, rabbit.port, rabbit.username, rabbit.password);
    const consulc: ConsulConfiguration = new ConsulConfiguration(consulconfig.address, consulconfig.port);
    const orbitalconfig: OrbitalConfiguration = new OrbitalConfiguration(rabbitconfig, consulc, input.servicename);

    it('has a getter method for rabbitmq configuration', () => {
        assert.equal(input.rabbitmq.address, orbitalconfig.rabbitmq.address);
    });
    it('has a getter method for consul configuration', () => {
        assert.equal(input.consul.port, orbitalconfig.consul.port);
    });
});

import { assert } from 'chai';
import { Rabbitmq } from '../../../../src/orbital-cli.domain/aggregates/publish-service-definition.aggregate/rabbitmq-configuration';

describe('rabbitMQ configuration', () => {
    const input = {
        address: 'localhost',
        password: 'guest',
        port: '8500',
        username: 'guest'
    };

    const rabbitconfig: Rabbitmq = new Rabbitmq(input.address, input.port, input.username, input.password);
    it('has a getter method for address', () => {
        assert.equal(input.address, rabbitconfig.address);
    });
    it('has a getter method for port', () => {
        assert.equal(input.port, rabbitconfig.port);
    });
    it('has a getter method for username', () => {
        assert.equal(input.username, rabbitconfig.username);
    });
    it('has a getter method for password', () => {
        assert.equal(input.password, rabbitconfig.password);
    });
});

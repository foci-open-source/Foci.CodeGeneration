import { assert } from 'chai';
import { UpdateService } from '../../../../src/orbital-cli.domain/aggregates/publish-service-definition.aggregate/updateservice';

describe('update service configuration', () => {
    const input = {
        servicename: 'exampleService'
    };

    const updateService: UpdateService = new UpdateService(input.servicename);
    it('has a getter method for servicename', () => {
        assert.equal(input.servicename, updateService.serviceName);
    });
});

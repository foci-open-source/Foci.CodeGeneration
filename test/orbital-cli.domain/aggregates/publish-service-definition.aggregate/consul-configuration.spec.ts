import { assert } from 'chai';
import { ConsulConfiguration } from '../../../../src/orbital-cli.domain/aggregates/publish-service-definition.aggregate/consul-configuration';

describe('consul configuration', () => {
    const input = {
        address: 'localhost',
        port: '8500'
    };

    const consulconfig: ConsulConfiguration = new ConsulConfiguration(input.address, input.port);
    it('has a getter method for address', () => {
        assert.equal(input.address, consulconfig.address);
    });
    it('has a getter method for port', () => {
        assert.equal(input.port, consulconfig.port);
    });
});

import { Operation } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/operation';
import { assert } from 'chai';
import { AdapterType } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';
import { Adapter } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter';
import { Schemas } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/schemas';
import { HTTPVerb } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/http-verb.enum';
import { Validate } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/validate-configuration';
import { Http } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/http-configuration';

describe('operation', () => {
    const input = {
        adapter: 'adapter',
        adapterConfig: 'configuration',
        adapterType: AdapterType.REST,
        httpEnabled: true,
        httpVerb: HTTPVerb.GET,
        httpVerbOnly: false,
        operationConfig: 'config',
        operationName: 'operation',
        request: 'request',
        response: 'response',
        translation: 'translation',
        validateRequest: true,
        validateResponse: true
    };

    const operation: Operation = new Operation(input.operationName);
    operation
        .addOperationConfiguration(input.operationConfig)
        .addAdapter(input.adapterType, input.adapterConfig, input.adapter)
        .addValidation(input.validateRequest, input.validateResponse)
        .addHttpConfiguration(input.httpEnabled, input.httpVerb, input.httpVerbOnly)
        .addSchemas(input.request, input.response)
        .addTranslation(input.translation);

    it('has a getter method for operation name', () => {
        assert.equal(input.operationName, operation.operationName);
    });

    it('has a getter method for operation config', () => {
        assert.equal(input.operationConfig, operation.operationConfiguration);
    });

    it('has a getter method for adapter', () => {
        const expect: Adapter = new Adapter(
            input.adapterType,
            input.adapterConfig,
            input.adapter
        );

        assert.deepEqual(expect, operation.adapter);
    });

    it('has a getter method for validate', () => {
        const expect: Validate = new Validate(
            input.validateRequest,
            input.validateResponse,
        );

        assert.deepEqual(expect.request, operation.validate.request);
        assert.deepEqual(expect.response, operation.validate.response);
    });

    it('has a getter method for http configuration', () => {
        const expect: Http = new Http(
            input.httpEnabled,
            input.httpVerb,
            input.httpVerbOnly
        );

        assert.deepEqual(expect.enabled, operation.httpConfiguration.enabled);
        assert.deepEqual(expect.verb, operation.httpConfiguration.verb);
        assert.deepEqual(expect.verbOnly, operation.httpConfiguration.verbOnly);
    });

    it('has a getter method for schemas', () => {
        const expect: Schemas = new Schemas(
            input.request,
            input.response
        );

        assert.deepEqual(expect, operation.schemas);
    });

    it('has a getter method for translation', () => {

        assert.deepEqual(input.translation, operation.translation);
    });

    it('has a getter method for boolean flag for operation sync or not', () => {
        const expect: boolean = true;
        assert.deepEqual(expect, operation.sync);
    });
});

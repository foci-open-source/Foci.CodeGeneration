import { Adapter } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter';
import { AdapterType } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';
import { assert } from 'chai';

describe('adapter', () => {
    const input = {
        adapter: 'adapter',
        adapterConfig: 'configuration',
        adapterType: AdapterType.REST
    };
    const adapter: Adapter = new Adapter(
        AdapterType.REST,
        input.adapterConfig,
        input.adapter
    );

    it('has a getter method for adapter string representation  ', () => {
        assert.equal(input.adapter, adapter.adapter);
    });

    it('has a getter method for adapter config ', () => {
        assert.equal(input.adapterConfig, adapter.configuration);
    });

    it('has a getter method for adapter type', () => {
        assert.equal(input.adapterType, adapter.type);
    });
});

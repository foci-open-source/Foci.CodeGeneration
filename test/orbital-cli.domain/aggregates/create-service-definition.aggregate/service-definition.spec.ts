import 'reflect-metadata';
import { assert } from 'chai';
import * as TypeMoq from 'typemoq';
import { ServiceDefinition } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/service-definition';
import { CreateServiceDefinitionRepository } from '../../../../src/orbital-cli.infrastructure/repositories/create-service-definition.repository';
import { ServiceDefinitionFactory } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/factories/service-definition.factory';
import { TemplateService } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/services/template.service';
import { AdapterType } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';
import { FileRepository } from '../../../../src/orbital-cli.infrastructure/repositories/file.repository';
import ARTIFACTS from '../../../../src/constants/artifacts';
import { Operation } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/operation';
import { HTTPVerb } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/http-verb.enum';
import { Validate } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/validate-configuration';
import { Http } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/http-configuration';

describe('Service Definition', () => {
    describe('When no services are created', () => {
        describe('A new service command is issued with a service name without an optional operation', () => {
            it('should create a service name', () => {
                let input = {
                    serviceName: 'MyNewService'
                };

                let expected = new ServiceDefinition(input.serviceName);

                let actual = new ServiceDefinition('MyNewService');

                assert.deepEqual(actual, expected);
            });

            it('should also have orbital file created with default values', () => {
                let templateService: TypeMoq.IMock<
                    TemplateService
                > = TypeMoq.Mock.ofType(TemplateService);

                let serviceDefinitionRepository: TypeMoq.IMock<
                    CreateServiceDefinitionRepository
                > = TypeMoq.Mock.ofType(CreateServiceDefinitionRepository);

                let serviceDefinitionFactory: ServiceDefinitionFactory = new ServiceDefinitionFactory(
                    templateService.object,
                    serviceDefinitionRepository.object
                );

                let input = {
                    orbitalFile:
                        '{"serviceName": "MyNewService","consul":{"address":"localhost","port":8000},"rabbit":{"address":"localhost",' +
                        '"port":5682,"username":"guest","password":"guest"}}',
                    serviceName: 'MyNewService'
                };

                templateService
                    .setup(it => {
                        it.renderOrbitalTemplate(input.serviceName);
                    })
                    .returns(() => input.orbitalFile);

                let expected = new ServiceDefinition(input.serviceName);
                expected.addOrbitalFile(input.orbitalFile);

                serviceDefinitionRepository
                    .setup(it => {
                        it.createServiceDefinition(
                            TypeMoq.It.isValue(expected)
                        );
                    })
                    .returns(() => expected);

                let actual = serviceDefinitionFactory.createServiceDefinition(
                    input.serviceName
                );
                assert.equal(expected.orbitalFile, actual.orbitalFile);
                assert.deepEqual(actual, expected);
            });
        });

        describe('A new service command is issued without a service name without an optional operation', () => {
            it('should throw an error with error message: "The service name can not be null"', () => {
                assert.throws(() => {
                    return new ServiceDefinition('');
                }, 'The service name can not be empty');
            });
        });

        describe('A new service command is issued without a service name without an optional operation', () => {
            it('should throw an error with error message: "The service name can not be null"', () => {
                const input = {
                    operationName: 'badOperation',
                    serviceName: 'goodService'
                };
                assert.throws(() => {
                    return new ServiceDefinition(
                        input.serviceName
                    ).getOperation(input.operationName);
                }, `When trying to retrieve operation ${input.operationName} from ${input.serviceName} the operation is undefined`);
            });
        });

        describe(
            'A new service command is issued with a service name ' +
                'and an optional operation flag and operation name',
            () => {
                it(
                    'should create a service definition and a ' +
                        'operation with default schema, adapter, ' +
                        'translation and operation configuration',
                    () => {
                        let serviceDefinitionRepository: TypeMoq.IMock<
                            CreateServiceDefinitionRepository
                        > = TypeMoq.Mock.ofType(
                            CreateServiceDefinitionRepository
                        );

                        let serviceDefinitionFactory: ServiceDefinitionFactory = new ServiceDefinitionFactory(
                            new TemplateService(),
                            serviceDefinitionRepository.object
                        );

                        let input = {
                            adapter: `{
    "type": "Adapters.Rest",
    "configuration":
        {
            "uri": "",
            "method": "GET",
            "headers": [
                {
                    "key": "Content-Type",
                    "value": "application/json"
                }
            ]
        }
}`,
                            adapterType: AdapterType.REST,
                            configuration: `{
            "uri": "",
            "method": "GET",
            "headers": [
                {
                    "key": "Content-Type",
                    "value": "application/json"
                }
            ]
        }`,
                            httpEnabled: true,
                            httpVerb: HTTPVerb.GET,
                            httpVerbOnly: false,
                            operationConfig: `{
    "sync": true,
    "validate": {
            "request": false,
            "response": false
        },
    "http": {
            "enabled": true,
            "verb": "GET",
            "verbOnly": false
        }
}`,
                            operationName: 'MyNewOperation',
                            orbitalFile: `{
    "serviceName": "MyNewService",
    "consul": {
        "address": "localhost",
        "port": 8500
    },
    "rabbit": {
        "address": "localhost",
        "port": 5672,
        "username": "guest",
        "password": "guest"
    }
}`,
                            schema: `{
    "title": "",
    "description": "",
    "type": "object",
    "typeName": "",
    "properties": {
        "id": {
            "type": "string"
        }
    }
}`,
                            serviceName: 'MyNewService',
                            sync: true,
                            translation: `function serviceInboundTranslation(e) {
    return {
        Id: e.id,
        Name: { FirstName: 'John', MiddleName: 'Jane', LastName: 'Doe' },
        Address: {
            StreeNumber: 5,
            AddressLine1: 'City Street',
            AddressLine2: '',
            City: 'Ottawa',
            ProvinceState: 'ON',
            Country: 'CA',
            PostalZipCode: '5A5A5A'
        }
    };
}
function getDynamicParameters(e) {
    var r = { customerId: e.id };
    return (
        47 == e.id &&
            throwBusinessError('Bad first and last names', {
                messageType: 'string'
            }),
        r
    );
}
function serviceOutboundTranslation(e) {
    return e;
}`,
                            validateRequest: false,
                            validateResponse: false
                        };
                        let operation: Operation = new Operation(
                            input.operationName
                        );

                        let expected = new ServiceDefinition(input.serviceName);
                        expected.addOrbitalFile(input.orbitalFile);
                        expected
                            .addOperation(input.operationName)
                            .addAdapter(
                                input.adapterType,
                                input.configuration,
                                input.adapter
                            )
                            .addValidation(
                                input.validateRequest,
                                input.validateResponse
                            )
                            .addHttpConfiguration(
                                input.httpEnabled,
                                input.httpVerb,
                                input.httpVerbOnly
                            )
                            .addSchemas(input.schema, input.schema)
                            .addTranslation(input.translation)
                            .addOperationConfiguration(input.operationConfig);

                        serviceDefinitionRepository
                            .setup(it => {
                                it.createServiceDefinition(
                                    TypeMoq.It.isValue(expected)
                                );
                            })
                            .returns(() => expected);

                        let actual = serviceDefinitionFactory.createServiceDefinitionWithOperation(
                            input.serviceName,
                            input.operationName,
                            input.adapterType,
                            input.sync,
                            input.validateRequest,
                            input.validateResponse,
                            input.httpEnabled,
                            input.httpVerb,
                            input.httpVerbOnly
                        );

                        assert.deepEqual(
                            actual.getOperation(input.operationName),
                            expected.getOperation(input.operationName)
                        );
                        assert.deepEqual(
                            actual.operations,
                            expected.operations
                        );
                        assert.deepEqual(actual, expected);
                    }
                );
            }
        );
    });

    describe('When a new service definition folder has been created', () => {
        describe('A new service command is issued with the same service name', () => {
            it('should throw an error message says the service name already exist.', () => {
                let fileRepository: TypeMoq.IMock<
                    FileRepository
                > = TypeMoq.Mock.ofType(FileRepository);

                let templateService: TypeMoq.IMock<
                    TemplateService
                > = TypeMoq.Mock.ofType(TemplateService);

                let input = {
                    orbitalFile:
                        '{"serviceName": "MyNewService","consul":{"address":"localhost","port":8000},"rabbit":{"address":"localhost",' +
                        '"port":5682,"username":"guest","password":"guest"}}',
                    serviceName: 'MyNewService'
                };

                fileRepository
                    .setup(it => {
                        it.checkExist(TypeMoq.It.isValue(input.serviceName));
                    })
                    .returns(() => true);

                let serviceDefinitionFactory: ServiceDefinitionFactory = new ServiceDefinitionFactory(
                    templateService.object,
                    new CreateServiceDefinitionRepository(fileRepository.object)
                );

                templateService
                    .setup(it => {
                        it.renderOrbitalTemplate(input.serviceName);
                    })
                    .returns(() => input.orbitalFile);

                let expected = new ServiceDefinition(input.serviceName);
                expected.addOrbitalFile(input.orbitalFile);

                console.assert(() => {
                    serviceDefinitionFactory.createServiceDefinition(
                        input.serviceName
                    );
                }, input.serviceName + ' already exists.');
            });
        });

        describe('A create new operation is issued with operation name, adapter type', () => {
            it(
                'should create a operation with with default schema, default translation, ' +
                    'and operation configuration',
                () => {
                    let serviceDefinitionRepository: TypeMoq.IMock<
                        CreateServiceDefinitionRepository
                    > = TypeMoq.Mock.ofType(CreateServiceDefinitionRepository);

                    let serviceDefinitionFactory: ServiceDefinitionFactory = new ServiceDefinitionFactory(
                        new TemplateService(),
                        serviceDefinitionRepository.object
                    );

                    let input = {
                        adapter: `{
"type": "Adapters.Rest",
"configuration":
    {
        "uri": "",
        "method": "GET",
        "headers": [
            {
                "key": "Content-Type",
                "value": "application/json"
            }
        ]
    }
}`,
                        adapterType: AdapterType.REST,
                        configuration: `{
        "uri": "",
        "method": "GET",
        "headers": [
            {
                "key": "Content-Type",
                "value": "application/json"
            }
        ]
    }`,
                        httpEnabled: true,
                        httpVerb: HTTPVerb.GET,
                        httpVerbOnly: false,
                        operationConfig: `{
"sync": true,
"validate": {
        "request": false,
        "response": false
    },
"http": {
        "enabled": true,
        "verb": "GET",
        "verbOnly": false
    }
}`,
                        operationName: 'MyNewOperation',
                        orbitalFile: `{
"serviceName": "MyNewService",
"consul": {
    "address": "localhost",
    "port": 8500
},
"rabbit": {
    "address": "localhost",
    "port": 5672,
    "username": "guest",
    "password": "guest"
}
}`,
                        schema: '',
                        serviceName: 'MyNewService',
                        sync: true,
                        translation: `function serviceInboundTranslation(message) {
var response = {};
return response;
}
function serviceOutboundTranslation(message) {
var response = {};
return response;
}
function getDynamicParameters(message) {
var messageParts = {};
return messageParts;
}`,
                        validateRequest: false,
                        validateResponse: false
                    };

                    serviceDefinitionRepository
                        .setup(it => {
                            it.getServiceDefinition();
                        })
                        .returns(
                            () => new ServiceDefinition(input.serviceName)
                        );

                    let expected = new ServiceDefinition(input.serviceName);

                    expected
                        .addOperation(input.operationName)
                        .addAdapter(
                            input.adapterType,
                            input.configuration,
                            input.adapter
                        )
                        .addValidation(
                            input.validateRequest,
                            input.validateResponse
                        )
                        .addHttpConfiguration(
                            input.httpEnabled,
                            input.httpVerb,
                            input.httpVerbOnly
                        )
                        .addSchemas(input.schema, input.schema)
                        .addTranslation(input.translation)
                        .addOperationConfiguration(input.operationConfig);

                    let actual = serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                        input.operationName,
                        input.adapterType,
                        input.sync,
                        input.validateRequest,
                        input.validateResponse,
                        input.httpEnabled,
                        input.httpVerb,
                        input.httpVerbOnly
                    );

                    assert.deepEqual(actual.orbitalFile, expected.orbitalFile);
                    assert.deepEqual(actual.serviceName, expected.serviceName);
                }
            );
        });

        describe('When a create operation command is issued with duplicate operation name', () => {
            it('should throw an error message says the operation name already exist.', () => {
                let fileRepository: TypeMoq.IMock<
                    FileRepository
                > = TypeMoq.Mock.ofType(FileRepository);

                let templateService: TypeMoq.IMock<
                    TemplateService
                > = TypeMoq.Mock.ofType(TemplateService);
                let input = {
                    adapter: 'adapter',
                    adapterType: AdapterType.REST,
                    configuration: 'config',
                    httpEnabled: true,
                    httpVerb: HTTPVerb.GET,
                    httpVerbOnly: false,
                    operationConfig: 'operationConfig',
                    operationName: 'MyNewOperation',
                    orbitalFile:
                        '{"serviceName": "MyNewService","consul":{"address":"localhost","port":8000},"rabbit":{"address":"localhost",' +
                        '"port":5682,"username":"guest","password":"guest"}}',
                    orbitalFilePath: ARTIFACTS.OrbitalFile,
                    schema: 'schema',
                    serviceName: 'MyNewService',
                    sync: false,
                    translation: 'translation',
                    validateRequest: true,
                    validateResponse: true
                };

                let operation: Operation = new Operation(input.operationName);

                templateService
                    .setup(it => {
                        it.renderDefaultAdapterConfigTemplate();
                    })
                    .returns(() => input.configuration);

                templateService
                    .setup(it => {
                        it.renderRestAdapterConfigTemplate();
                    })
                    .returns(() => input.adapter);

                templateService
                    .setup(it => {
                        it.renderDefaultSchemaTemplate();
                    })
                    .returns(() => input.schema);

                templateService
                    .setup(it => {
                        it.renderDefaultTranslationTemplate();
                    })
                    .returns(() => input.translation);

                templateService
                    .setup(it => {
                        it.renderOperationConfigurationTemplate(
                            new Http(
                                input.httpEnabled,
                                input.httpVerb,
                                input.httpVerbOnly
                            ),
                            input.sync,
                            new Validate(
                                input.validateRequest,
                                input.validateResponse
                            )
                        );
                    })
                    .returns(() => input.operationConfig);

                let serviceDefinitionFactory: ServiceDefinitionFactory = new ServiceDefinitionFactory(
                    templateService.object,
                    new CreateServiceDefinitionRepository(fileRepository.object)
                );

                fileRepository
                    .setup(it => {
                        it.checkExist(TypeMoq.It.isValue(input.operationName));
                    })
                    .returns(() => true);

                fileRepository
                    .setup(it => {
                        it.checkExist(
                            TypeMoq.It.isValue(input.orbitalFilePath)
                        );
                    })
                    .returns(() => true);

                fileRepository
                    .setup(it => {
                        it.readFile(TypeMoq.It.isValue(input.orbitalFilePath));
                    })
                    .returns(() => input.orbitalFile);

                let expected = new ServiceDefinition(input.serviceName);
                expected.addOperation(input.operationName);

                assert.throws(() => {
                    serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                        input.operationName,
                        input.adapterType,
                        input.sync,
                        input.validateRequest,
                        input.validateResponse,
                        input.httpEnabled,
                        input.httpVerb,
                        input.httpVerbOnly
                    );
                }, `Operation ${input.operationName} already exists`);
            });

            describe('When a create operation command is issued with operation name but without orbital file', () => {
                it('should throw an error message says Cannot create the operation without the service, please create a service first.', () => {
                    let fileRepository: TypeMoq.IMock<
                        FileRepository
                    > = TypeMoq.Mock.ofType(FileRepository);

                    let templateService: TypeMoq.IMock<
                        TemplateService
                    > = TypeMoq.Mock.ofType(TemplateService);

                    let input = {
                        adapter: 'adapter',
                        adapterType: AdapterType.REST,
                        configuration: 'config',
                        httpEnabled: true,
                        httpVerb: HTTPVerb.GET,
                        httpVerbOnly: false,
                        operationConfig: 'operationConfig',
                        operationName: 'MyNewOperation',
                        orbitalFilePath: ARTIFACTS.OrbitalFile,
                        schema: 'schema',
                        serviceName: 'MyNewService',
                        sync: false,
                        translation: 'translation',
                        validateRequest: true,
                        validateResponse: true
                    };

                    let operation: Operation = new Operation(
                        input.operationName
                    );

                    templateService
                        .setup(it => {
                            it.renderDefaultAdapterConfigTemplate();
                        })
                        .returns(() => input.configuration);

                    templateService
                        .setup(it => {
                            it.renderRestAdapterConfigTemplate();
                        })
                        .returns(() => input.adapter);

                    templateService
                        .setup(it => {
                            it.renderDefaultSchemaTemplate();
                        })
                        .returns(() => input.schema);

                    templateService
                        .setup(it => {
                            it.renderDefaultTranslationTemplate();
                        })
                        .returns(() => input.translation);

                    templateService
                        .setup(it => {
                            it.renderOperationConfigurationTemplate(
                                new Http(
                                    input.httpEnabled,
                                    input.httpVerb,
                                    input.httpVerbOnly
                                ),
                                input.sync,
                                new Validate(
                                    input.validateRequest,
                                    input.validateResponse
                                )
                            );
                        })
                        .returns(() => input.operationConfig);

                    let serviceDefinitionFactory: ServiceDefinitionFactory = new ServiceDefinitionFactory(
                        templateService.object,
                        new CreateServiceDefinitionRepository(
                            fileRepository.object
                        )
                    );

                    fileRepository
                        .setup(it => {
                            it.checkExist(
                                TypeMoq.It.isValue(input.operationName)
                            );
                        })
                        .returns(() => true);

                    let expected = new ServiceDefinition(input.serviceName);
                    expected.addOperation(input.operationName);

                    assert.throws(() => {
                        serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                            input.operationName,
                            input.adapterType,
                            input.sync,
                            input.validateRequest,
                            input.validateResponse,
                            input.httpEnabled,
                            input.httpVerb,
                            input.httpVerbOnly
                        );
                    }, `Cannot create the operation without the service, please create a service first.`);
                });
            });
        });
    });
});

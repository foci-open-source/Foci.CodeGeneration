import { Schemas } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/schemas';
import { assert } from 'chai';
describe('schemas', () => {
    const input = {
        request: 'request',
        response: 'response'
    };
    const schemas: Schemas = new Schemas(input.request, input.response);

    it('has a getter method for request string representation  ', () => {
        assert.equal(input.request, schemas.request);
    });

    it('has a getter method for response string representation  ', () => {
        assert.equal(input.response, schemas.response);
    });
});

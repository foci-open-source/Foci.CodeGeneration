import { assert } from 'chai';
import { Operation } from '../../../../src/orbital-cli.domain/aggregates/build-service-definition.aggregate/operation';
import { Adapter } from '../../../../src/orbital-cli.domain/aggregates/build-service-definition.aggregate/adapter';
import { Schemas } from '../../../../src/orbital-cli.domain/aggregates/build-service-definition.aggregate/schemas';


describe('operation', () => {
    const input = {
        adapterConfig: 'configuration',
        adapterType: 'rest',
        operationConfig: 'config',
        operationName: 'operation',
        request: 'request',
        response: 'response',
        sync: true,
        translation: 'translation'
    };

    const operation: Operation = new Operation(input.operationName);
    operation
        .addOperationSync(input.sync)
        .addAdapter(input.adapterType, input.adapterConfig)
        .addSchemas(input.request, input.response)
        .addTranslation(input.translation);

    it('has a getter method for operation name', () => {
        assert.equal(input.operationName, operation.operationName);
    });

    it('has a getter method for adapter', () => {
        const expect: Adapter = new Adapter(
            input.adapterType,
            input.adapterConfig,
        );

        assert.deepEqual(expect, operation.adapter);
    });

    it('has a getter method for schemas', () => {
        const expect: Schemas = new Schemas(
            input.request,
            input.response
        );

        assert.deepEqual(expect, operation.schemas);
    });

    it('has a getter method for translation', () => {

        assert.deepEqual(input.translation, operation.translation);
    });

    it('has a getter method for boolean flag for operation sync or not', () => {
        const expect: boolean = true;
        assert.deepEqual(expect, operation.sync);
    });
});

import { AdapterType } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';
import { Operation } from '../../../../src/orbital-cli.domain/aggregates/build-service-definition.aggregate/operation';
import { Adapter } from '../../../../src/orbital-cli.domain/aggregates/build-service-definition.aggregate/adapter';
import { Schemas } from '../../../../src/orbital-cli.domain/aggregates/build-service-definition.aggregate/schemas';
import { ServiceDefinition } from '../../../../src/orbital-cli.domain/aggregates/build-service-definition.aggregate/service-definition';
import { assert } from 'chai';
import { HTTPVerb } from '../../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/http-verb.enum';
describe('Service Definition', () => {
    describe('with build command issued', () => {
        it('should generate the built service definition json', () => {
            const input = {
                adapter: new Adapter(AdapterType.REST, 'adapterConfig'),
                httpEnabled: true,
                operation: new Operation('GoodOperation'),
                schemas: new Schemas('request', 'response'),
                serviceName: 'goodService',
                sync: true,
                translation: 'translation',
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.GET,
                verbOnly: true
            };

            let serviceDefinition: ServiceDefinition = new ServiceDefinition(
                input.serviceName
            );
            serviceDefinition
                .addOperation(input.operation.operationName)
                .addAdapter(input.adapter.type, input.adapter.configuration)
                .addHttpConfiguration(
                    input.httpEnabled,
                    input.verb,
                    input.verbOnly
                )
                .addValidation(input.validateRequest, input.validateResponse)
                .addSchemas(input.schemas.request, input.schemas.response)
                .addOperationSync(input.sync)
                .addTranslation(input.translation);

            const actual = JSON.stringify(serviceDefinition);

            const expect: string =
                `{"operations":[{"adapter":{"configuration":"adapterConfig","type":"Adapters.Rest"},"http":{"enabled":true,"verb":"GET","verbOnly` +
                `":true},"name":"GoodOperation","schemas":{"request":"request","response":"response"},"sync":true,"translation":"translation","validate":{"request":true,` +
                `"response":true}}],"serviceName":"goodService"}`;

            assert.equal(serviceDefinition.serviceName, input.serviceName);
            assert.equal(expect, actual);
        });
    });
});

import 'reflect-metadata';
import * as TypeMoq from 'typemoq';
import { assert } from 'chai';
import { TemplateService } from '../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/services/template.service';
import { FileRepository } from '../../../src/orbital-cli.infrastructure/repositories/file.repository';
import { Operation } from '../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/operation';
import { HTTPVerb } from '../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/http-verb.enum';
import { Http } from '../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/http-configuration';
import { Validate } from '../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/validate-configuration';

describe('Template services', () => {
    let templateService: TemplateService;

    beforeEach(() => {
        templateService = new TemplateService();
    });

    describe('render orbital template', () => {
        it('should successfully render the template file to string', () => {
            let input = {
                orbitalFileTemplateString: `{
    "serviceName": "testService",
    "consul": {
        "address": "localhost",
        "port": 8500
    },
    "rabbit": {
        "address": "localhost",
        "port": 5672,
        "username": "guest",
        "password": "guest"
    }
}`,
                serviceName: 'testService'
            };

            let expect = input.orbitalFileTemplateString;

            let actual = templateService.renderOrbitalTemplate(
                input.serviceName
            );

            assert.equal(expect, actual);
        });
    });

    describe('render default adapter config template', () => {
        it('should successfully render the template file to string', () => {
            let input = {
                configurationTemplateString: `{
            "uri": "",
            "method": "GET",
            "headers": [
                {
                    "key": "Content-Type",
                    "value": "application/json"
                }
            ]
        }`
            };

            let expect = input.configurationTemplateString;

            let actual = templateService.renderDefaultAdapterConfigTemplate();

            assert.equal(expect, actual);
        });
    });

    describe('render default schema template', () => {
        it('should successfully render the template file to string', () => {
            let input = {
                schemaTemplateString: `{
    "title": "",
    "description": "",
    "type": "object",
    "typeName": "",
    "properties": {
        "id": {
            "type": "string"
        }
    }
}`
            };
            let expect = input.schemaTemplateString;

            let actual = templateService.renderDefaultSchemaTemplate();

            assert.equal(expect, actual);
        });
    });

    describe('render default translation template', () => {
        it('should successfully render the template file to string', () => {
            let input = {
                translationTemplateString: `function serviceInboundTranslation(e) {
    return {
        Id: e.id,
        Name: { FirstName: 'John', MiddleName: 'Jane', LastName: 'Doe' },
        Address: {
            StreeNumber: 5,
            AddressLine1: 'City Street',
            AddressLine2: '',
            City: 'Ottawa',
            ProvinceState: 'ON',
            Country: 'CA',
            PostalZipCode: '5A5A5A'
        }
    };
}
function getDynamicParameters(e) {
    var r = { customerId: e.id };
    return (
        47 == e.id &&
            throwBusinessError('Bad first and last names', {
                messageType: 'string'
            }),
        r
    );
}
function serviceOutboundTranslation(e) {
    return e;
}`
            };

            let expect = input.translationTemplateString;

            let actual = templateService.renderDefaultTranslationTemplate();

            assert.equal(expect, actual);
        });
    });

    describe('render sync operation configuration template', () => {
        it('should successfully render the template file to string with the default operation sync type', () => {
            let input = {
                operationConfigurationString: `{
    "sync": true,
    "validate": {
            "request": true,
            "response": true
        },
    "http": {
            "enabled": true,
            "verb": "GET",
            "verbOnly": true
        }
}`,
                operationName: `MyOperationName`
            };

            let expect = input.operationConfigurationString;

            let actual = templateService.renderOperationConfigurationTemplate(
                new Http(true, HTTPVerb.GET, true),
                true,
                new Validate(true, true)
            );

            assert.equal(actual, expect);
        });
    });

    describe('render rest adapter config template', () => {
        it('should successfully render the template file to string', () => {
            let input = {
                adapterConfigString: `{
    "type": "Adapters.Rest",
    "configuration":
        {
            "uri": "",
            "method": "GET",
            "headers": [
                {
                    "key": "Content-Type",
                    "value": "application/json"
                }
            ]
        }
}`
            };

            let expect = input.adapterConfigString;

            let actual = templateService.renderRestAdapterConfigTemplate();

            assert.equal(expect, actual);
        });
    });

    describe('render mock adapter config template', () => {
        it('should successfully render the template file to string', () => {
            let input = {
                adapterConfigString:
                    `{
    "type": "Adapters.Mock",
    "configuration":
        {"selector":"$.Id","conditions":[{"operator":"==","compareTo":"48","exception":"What is ` +
                    `this"},{"operator":"==","compareTo":"112","response":"{\\"id\\":112,\\"name\\":{\\"firstName\\":\\"` +
                    `John\\",\\"middleName\\":\\"Jane\\",\\"lastName\\":\\"Doe\\"},\\"address\\":{\\"addressLine1\\":\\"5 City ` +
                    `Street\\",\\"addressLine2\\":\\"\\",\\"city\\":\\"Ottawa\\",\\"provinceState\\":\\"ON\\",\\"country\\":\\"CA\\",\\"` +
                    `postalZipCode\\":\\"5A5A5A\\"}}"},{"operator":">","compareTo":"73","response":"{\\"id\\":24,\\"name\\":{\\"` +
                    `firstName\\":\\"John\\",\\"middleName\\":\\"Not\\",\\"lastName\\":\\"Smith\\"},\\"address\\":{\\"streetNumber` +
                    `\\":123,\\"addressLine1\\":\\"East Lake Forest Street\\",\\"city\\":\\"Fishingdale\\",\\"provinceState\\":\\"` +
                    `NY\\",\\"country\\":\\"US\\",\\"postalZipCode\\":\\"54321\\"}}"},{"operator":"<","compareTo":"73","response` +
                    `":"{\\"id\\":42,\\"name\\":{\\"firstName\\":\\"John\\",\\"middleName\\":\\"Ofcourse\\",\\"lastName\\":\\"Smith\\"},\\"` +
                    `address\\":{\\"streetNumber\\":694,\\"addressLine1\\":\\"North Lake Forest Street\\",\\"city\\":\\"Farmingdal` +
                    `e\\",\\"provinceState\\":\\"NY\\",\\"country\\":\\"US\\",\\"postalZipCode\\":\\"11735\\"}}"}],"default":{"response"` +
                    `: "{\\"response\\": \\"Default Response\\"}"}}
}`
            };

            let expect = input.adapterConfigString;

            let actual = templateService.renderMockAdapterConfigTemplate();

            assert.equal(actual, expect);
        });
    });

    describe('render database adapter config template', () => {
        it('should successfully render the template file to string', () => {
            let input = {
                adapterConfigString:
                    `{
    "type": "Adapters.Database",
    "configuration":
        {"statement":"EXECUTE","type":"MYSQL","sql":"INSERT INTO Customer ( CustomerId, LastName, FirstName ) VALUES ` +
                    `( @id, @lastName, @firstName );","connection":"Server=localhost;Database=Customer;Uid=root;Pwd=example;","parameters":` +
                    `{"id":"1","lastName":"Goodman","firstName":"Saul"}}
}`
            };

            let expect = input.adapterConfigString;

            let actual = templateService.renderDatabaseAdapterConfigTemplate();

            assert.equal(actual, expect);
        });
    });
});

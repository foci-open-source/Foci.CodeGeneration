import 'reflect-metadata';
import { assert } from 'chai';
import { ConsulRepository } from '../../../src/orbital-cli.infrastructure/repositories/consul.repository';
import { OrbitalConfiguration } from '../../../src/orbital-cli.domain/aggregates/publish-service-definition.aggregate/orbital-configuration';
import * as TypeMoq from 'typemoq';
import { Rabbitmq } from '../../../src/orbital-cli.domain/aggregates/publish-service-definition.aggregate/rabbitmq-configuration';
import { ConsulConfiguration } from '../../../src/orbital-cli.domain/aggregates/publish-service-definition.aggregate/consul-configuration';

describe('publish to consul', () => {
    const input = {
        servicedefinition: 'someservice',
        servicename: 'exampleService'
    };

     const rabbitmq: Rabbitmq = new Rabbitmq('mockIp', '6720', 'myname', 'mypass');
     const consul: ConsulConfiguration = new ConsulConfiguration('mockip', '5674');
    const orbitalconfig: OrbitalConfiguration = new OrbitalConfiguration(rabbitmq, consul, input.servicename);
    const consulRepo: ConsulRepository = new ConsulRepository();
    it('setting KV pair fails', async () => {
        consulRepo.createConsulConnection(orbitalconfig);

        assert.isFalse(await consulRepo.publishServiceDefinition(input.servicedefinition, input.servicename));
    });
});

describe('publish to consul', () => {
    let consulRep: TypeMoq.IMock<ConsulRepository>;
    const input = {
        servicedefinition: 'someservice',
        servicename: 'exampleService'
    };
    const rabbitmq: Rabbitmq = new Rabbitmq('mockIp', '6720', 'myname', 'mypass');
    const consul: ConsulConfiguration = new ConsulConfiguration('mockip', '5674');
   const orbitalconfig: OrbitalConfiguration = new OrbitalConfiguration(rabbitmq, consul, input.servicename);
    beforeEach(() => {
        consulRep = TypeMoq.Mock.ofType(ConsulRepository);
    });

    it('setting KV pair succeeds', () => {
        consulRep.setup(it => {
            it.publishServiceDefinition(TypeMoq.It.isValue(input.servicedefinition), TypeMoq.It.isValue(input.servicename));
        })
        .returns(() => true);

        consulRep.object.createConsulConnection(orbitalconfig);

        assert.isTrue(consulRep.object.publishServiceDefinition(input.servicedefinition, input.servicename));
    });
});

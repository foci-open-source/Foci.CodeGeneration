import { cliContainer } from '../../src/inversify.config';
import { AdapterType } from '../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';
import { assert } from 'chai';
import * as fs from 'fs';
import * as path from 'path';
import ARTIFACTS from '../../src/constants/artifacts';
import { ServiceDefinitionFactory } from '../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/factories/service-definition.factory';
import { HTTPVerb } from '../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/http-verb.enum';

describe('Running create service command with only service name then running create operation command', () => {
    const serviceDefinitionFactory = cliContainer.resolve<
        ServiceDefinitionFactory
    >(ServiceDefinitionFactory);

    const serviceName = 'ServeGoodman';
    beforeEach(() => {
        serviceDefinitionFactory.createServiceDefinition(serviceName);
        process.chdir(serviceName);
    });
    afterEach(() => {
        process.chdir('../');
        deleteFolderRecursive(serviceName);
    });

    describe('with operation name and adapter type flag', () => {
        it('should create the operation folder', () => {
            const operationName = 'goodOperation';

            let input = {
                adapterType: AdapterType.REST,
                httpEnable: false,
                operationFolderPath: path.join(operationName),
                sync: false,
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );
            assert.isTrue(fs.existsSync(input.operationFolderPath));
        });

        it('should create schemas folder inside the operation folder', () => {
            const operationName = 'goodOperation';

            let input = {
                adapterType: AdapterType.REST,
                httpEnable: false,
                operationFolderPath: path.join(operationName),
                schemaFolderPath: path.join(
                    operationName,
                    ARTIFACTS.SchemasFolder
                ),
                sync: false,
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.schemaFolderPath));
        });

        it('should create default request and response schema inside the schemas folder', () => {
            const operationName = 'goodOperation';
            let input = {
                adapterType: AdapterType.REST,
                expectSchemaPath:
                    '../test/integration-test/expect-result/schema.json',
                httpEnable: false,
                requestSchemaFilePath: path.join(
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.RequestSchemaFile
                ),
                responseSchemaFilePath: path.join(
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.ResponseSchemaFile
                ),
                sync: false,
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.requestSchemaFilePath));
            assert.isTrue(fs.existsSync(input.responseSchemaFilePath));

            let actualRequest = fs.readFileSync(
                input.requestSchemaFilePath,
                'utf8'
            );

            let actualResponse = fs.readFileSync(
                input.responseSchemaFilePath,
                'utf8'
            );

            let expect = fs.readFileSync(input.expectSchemaPath, 'utf8');

            assert.deepEqual(expect, actualRequest);
            assert.deepEqual(expect, actualResponse);
        });

        it('should create translations folder inside the operation folder', () => {
            const operationName = 'goodOperation';
            let input = {
                adapterType: AdapterType.REST,
                httpEnable: false,
                sync: false,
                translationFolderPath: path.join(
                    operationName,
                    ARTIFACTS.TranslationsFolder
                ),
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.translationFolderPath));
        });

        it('should create adapter configurations folder inside the operation folder', () => {
            const operationName = 'goodOperation';
            let input = {
                adapterType: AdapterType.REST,
                configurationFolderPath: path.join(
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder
                ),
                httpEnable: false,
                sync: false,
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.configurationFolderPath));
        });

        it('should create rest adapter config file inside the configurations folder', () => {
            const operationName = 'goodOperation';
            let input = {
                adapterConfigFilePath: path.join(
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder,
                    ARTIFACTS.AdapterConfigFile
                ),
                adapterType: AdapterType.REST,
                expectRestAdapterPath:
                    '../test/integration-test/expect-result/rest-adapter-config.json',
                httpEnable: false,
                sync: false,
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.adapterConfigFilePath));

            let actual = fs.readFileSync(input.adapterConfigFilePath, 'utf8');

            let expect = fs.readFileSync(input.expectRestAdapterPath, 'utf8');

            assert.equal(expect, actual);
        });

        it('should create operationConfiguration file inside the operation folder', () => {
            const operationName = 'goodOperation';
            let input = {
                adapterType: AdapterType.REST,
                expectDefaultOperationConfigurationPath:
                    '../test/integration-test/expect-result/sync-operation-configuration.json',
                httpEnable: false,
                operationConfigurationFilePath: path.join(
                    operationName,
                    ARTIFACTS.OperationConfigurationFile
                ),
                sync: true,
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.operationConfigurationFilePath));

            let actual = fs.readFileSync(
                input.operationConfigurationFilePath,
                'utf8'
            );

            let expect = fs.readFileSync(
                input.expectDefaultOperationConfigurationPath,
                'utf8'
            );

            assert.deepEqual(expect, actual);
        });
    });

    describe('with operation name, service name, adapter type flag and async operation type flag', () => {
        it('should create operationConfiguration file inside the operation folder', () => {
            const operationName = 'goodOperation';

            let input = {
                adapterType: AdapterType.REST,
                expectAsyncOperationConfigurationPath:
                    '../test/integration-test/expect-result/async-operation-configuration.json',
                httpEnable: false,
                operationConfigurationFilePath: path.join(
                    operationName,
                    ARTIFACTS.OperationConfigurationFile
                ),
                operationFolderPath: path.join(serviceName, operationName),
                sync: false,
                validateRequest: false,
                validateResponse: false,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.operationConfigurationFilePath));

            let actual = fs.readFileSync(
                input.operationConfigurationFilePath,
                'utf8'
            );

            let expect = fs.readFileSync(
                input.expectAsyncOperationConfigurationPath,
                'utf8'
            );

            assert.deepEqual(expect, actual);
        });
    });
});

const deleteFolderRecursive = function(inputPath: string) {
    let files: string[] = [];
    if (fs.existsSync(inputPath)) {
        files = fs.readdirSync(inputPath);
        files.forEach(function(file, index) {
            let curPath = inputPath + '/' + file;
            if (fs.lstatSync(curPath).isDirectory()) {
                deleteFolderRecursive(curPath);
            } else {
                // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(inputPath);
    }
};

function serviceInboundTranslation(e) {
    return {
        Id: e.id,
        Name: { FirstName: 'John', MiddleName: 'Jane', LastName: 'Doe' },
        Address: {
            StreeNumber: 5,
            AddressLine1: 'City Street',
            AddressLine2: '',
            City: 'Ottawa',
            ProvinceState: 'ON',
            Country: 'CA',
            PostalZipCode: '5A5A5A'
        }
    };
}
function getDynamicParameters(e) {
    var r = { customerId: e.id };
    return (
        47 == e.id &&
            throwBusinessError('Bad first and last names', {
                messageType: 'string'
            }),
        r
    );
}
function serviceOutboundTranslation(e) {
    return e;
}
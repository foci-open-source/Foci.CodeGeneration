import { cliContainer } from '../../src/inversify.config';
import { assert } from 'chai';
import * as fs from 'fs';
import * as path from 'path';
import ARTIFACTS from '../../src/constants/artifacts';
import { ServiceDefinitionFactory } from '../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/factories/service-definition.factory';
import { AdapterType } from '../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';
import { HTTPVerb } from '../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/http-verb.enum';

describe('Running create service command', () => {
    const serviceDefinitionFactory = cliContainer.resolve<
        ServiceDefinitionFactory
    >(ServiceDefinitionFactory);

    const serviceName = 'ServeGoodman';

    afterEach(() => {
        deleteFolderRecursive(serviceName);
    });

    describe('with only service name', () => {
        it('should create the service name folder', () => {
            serviceDefinitionFactory.createServiceDefinition(serviceName);

            assert.isTrue(fs.existsSync(serviceName));
        });

        it('should create the orbital file inside the service folder', () => {
            let input = {
                expectOrbitalPath:
                    './test/integration-test/expect-result/orbital.json',
                orbitalFilePath: path.join(serviceName, ARTIFACTS.OrbitalFile)
            };

            serviceDefinitionFactory.createServiceDefinition(serviceName);

            assert.isTrue(fs.existsSync(input.orbitalFilePath));

            let actual = fs.readFileSync(input.orbitalFilePath, 'utf8');
            let expect = fs.readFileSync(input.expectOrbitalPath, 'utf8');

            assert.deepEqual(expect, actual);
        });
    });

    describe('with service name and operation name', () => {
        const operationName = 'goodOperation';
        it('should create the service name and the operation folder', () => {
            let input = {
                adapterType: AdapterType.REST,
                httpEnable: false,
                operationFolderPath: path.join(serviceName, operationName),
                sync: false,
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.operationFolderPath));
        });

        it('should create schemas folder inside the operation folder', () => {
            let input = {
                adapterType: AdapterType.REST,
                httpEnable: false,
                schemaFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder
                ),
                sync: false,
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.schemaFolderPath));
        });

        it('should create default request and response schema inside the schemas folder', () => {
            let input = {
                adapterType: AdapterType.REST,
                expectSchemaPath:
                    './test/integration-test/expect-result/schema.json',
                httpEnable: false,
                requestSchemaFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.RequestSchemaFile
                ),
                responseSchemaFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.ResponseSchemaFile
                ),
                sync: false,
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.requestSchemaFilePath));
            assert.isTrue(fs.existsSync(input.responseSchemaFilePath));

            let actualRequest = fs.readFileSync(
                input.requestSchemaFilePath,
                'utf8'
            );

            let actualResponse = fs.readFileSync(
                input.responseSchemaFilePath,
                'utf8'
            );

            let expect = fs.readFileSync(input.expectSchemaPath, 'utf8');

            assert.deepEqual(expect, actualRequest);
            assert.deepEqual(expect, actualResponse);
        });

        it('should create translations folder inside the operation folder', () => {
            let input = {
                adapterType: AdapterType.REST,
                httpEnable: false,
                sync: false,
                translationFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.TranslationsFolder
                ),
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.translationFolderPath));
        });

        it('should create the default translation file inside the translations folder', () => {
            let input = {
                adapterType: AdapterType.REST,
                expectTranslation:
                    './test/integration-test/expect-result/translation.js',
                httpEnable: false,
                sync: false,
                translationFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.TranslationsFolder,
                    ARTIFACTS.TranslationFile
                ),
                validateRequest: true,
                validateResponse: true,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.translationFilePath));

            let actualTranslation = fs.readFileSync(
                input.translationFilePath,
                'utf8'
            );

            let expectTranslation = fs.readFileSync(
                input.expectTranslation,
                'utf8'
            );

            assert.equal(expectTranslation, actualTranslation);
        });

        it('should create adapter configurations folder inside the operation folder', () => {
            let input = {
                adapterType: AdapterType.REST,
                configurationFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder
                ),
                httpEnable: false,
                sync: false,
                validateRequest: false,
                validateResponse: false,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.configurationFolderPath));
        });

        it('should create default rest adapter config file inside the configurations folder', () => {
            let input = {
                adapterConfigFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder,
                    ARTIFACTS.AdapterConfigFile
                ),
                adapterType: AdapterType.REST,
                expectRestAdapterPath:
                    './test/integration-test/expect-result/rest-adapter-config.json',
                httpEnable: false,
                sync: false,
                validateRequest: false,
                validateResponse: false,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.adapterConfigFilePath));

            let actual = fs.readFileSync(input.adapterConfigFilePath, 'utf8');

            let expect = fs.readFileSync(input.expectRestAdapterPath, 'utf8');

            assert.equal(expect, actual);
        });

        it('should create operationConfiguration file inside the operation folder', () => {
            let input = {
                adapterType: AdapterType.REST,
                expectDefaultOperationConfigurationPath:
                    './test/integration-test/expect-result/async-operation-configuration.json',
                httpEnable: false,
                operationConfigurationFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.OperationConfigurationFile
                ),
                sync: false,
                validateRequest: false,
                validateResponse: false,
                verb: HTTPVerb.NONE,
                verbOnly: false
            };

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                input.adapterType,
                input.sync,
                input.validateRequest,
                input.validateResponse,
                input.httpEnable,
                input.verb,
                input.verbOnly
            );

            assert.isTrue(fs.existsSync(input.operationConfigurationFilePath));

            let actual = fs.readFileSync(
                input.operationConfigurationFilePath,
                'utf8'
            );

            let expect = fs.readFileSync(
                input.expectDefaultOperationConfigurationPath,
                'utf8'
            );

            assert.equal(expect, actual);
        });
    });
});

const deleteFolderRecursive = function(inputPath: string) {
    let files: string[] = [];
    if (fs.existsSync(inputPath)) {
        files = fs.readdirSync(inputPath);
        files.forEach(function(file, index) {
            let curPath = inputPath + '/' + file;
            if (fs.lstatSync(curPath).isDirectory()) {
                deleteFolderRecursive(curPath);
            } else {
                // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(inputPath);
    }
};

import { cliContainer } from '../../../src/inversify.config';
import { ServiceDefinitionFactory } from '../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/factories/service-definition.factory';
import * as fs from 'fs';
import * as path from 'path';
import { test } from '@oclif/test';
import { assert } from 'chai';
import ARTIFACTS from '../../../src/constants/artifacts';
import { AdapterType } from '../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';

describe('Create operation command', () => {
    const serviceName = 'ServeGoodman';
    const serviceDefinitionFactory = cliContainer.resolve<
        ServiceDefinitionFactory
    >(ServiceDefinitionFactory);
    beforeEach(() => {
        serviceDefinitionFactory.createServiceDefinition(serviceName);
        process.chdir(serviceName);
    });
    afterEach(() => {
        process.chdir('../');
        deleteFolderRecursive(serviceName);
    });

    describe('create async operation with rest adapter', () => {
        const operationName = 'goodOperation';

        let input = {
            adapterConfigFilePath: path.join(
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder,
                ARTIFACTS.AdapterConfigFile
            ),
            configurationFolderPath: path.join(
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder
            ),
            expectDefaultOperationConfigurationPath:
                '../test/integration-test/expect-result/async-operation-configuration.json',
            expectRestAdapterPath:
                '../test/integration-test/expect-result/rest-adapter-config.json',
            expectSchemaPath:
                '../test/integration-test/expect-result/schema.json',
            expectTranslation:
                '../test/integration-test/expect-result/translation.js',
            operationConfigurationFilePath: path.join(
                operationName,
                ARTIFACTS.OperationConfigurationFile
            ),
            operationFolderPath: path.join(operationName),
            requestSchemaFilePath: path.join(
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.RequestSchemaFile
            ),
            responseSchemaFilePath: path.join(
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.ResponseSchemaFile
            ),
            schemaFolderPath: path.join(operationName, ARTIFACTS.SchemasFolder),
            translationFilePath: path.join(
                operationName,
                ARTIFACTS.TranslationsFolder,
                ARTIFACTS.TranslationFile
            ),
            translationFolderPath: path.join(
                operationName,
                ARTIFACTS.TranslationsFolder
            )
        };

        test.stdout()
            .command([
                'create:operation',
                operationName,
                '-s',
                'false',
                '-a',
                AdapterType.REST,
                '--validateRequest',
                'false',
                '--validateResponse',
                'false',
                '--httpEnabled',
                'NONE',
                '--verbOnly',
                'false'
            ])
            .it(
                'runs create:operation goodOperation -s false --validateRequest false --validateResponse false --httpEnabled NONE',
                ctx => {
                    let actualTranslation = fs.readFileSync(
                        input.translationFilePath,
                        'utf8'
                    );

                    let expectTranslation = fs.readFileSync(
                        input.expectTranslation,
                        'utf8'
                    );

                    let actualAdapterConfigFile = fs.readFileSync(
                        input.adapterConfigFilePath,
                        'utf8'
                    );

                    let expectRestAdapter = fs.readFileSync(
                        input.expectRestAdapterPath,
                        'utf8'
                    );

                    let actualDefaultOperationConfiguration = fs.readFileSync(
                        input.operationConfigurationFilePath,
                        'utf8'
                    );

                    let expectDefaultOperationConfiguration = fs.readFileSync(
                        input.expectDefaultOperationConfigurationPath,
                        'utf8'
                    );

                    assert.isTrue(fs.existsSync(input.operationFolderPath));

                    assert.isTrue(fs.existsSync(input.translationFolderPath));

                    assert.equal(expectTranslation, actualTranslation);

                    assert.isTrue(fs.existsSync(input.configurationFolderPath));

                    assert.isTrue(fs.existsSync(input.adapterConfigFilePath));

                    assert.equal(expectRestAdapter, actualAdapterConfigFile);

                    assert.isTrue(
                        fs.existsSync(input.operationConfigurationFilePath)
                    );

                    assert.equal(
                        expectDefaultOperationConfiguration,
                        actualDefaultOperationConfiguration
                    );
                }
            );
    });

    describe('create sync operation with validation request and validation response and rest adapter', () => {
        const operationName = 'goodOperation';

        let input = {
            adapterConfigFilePath: path.join(
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder,
                ARTIFACTS.AdapterConfigFile
            ),
            configurationFolderPath: path.join(
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder
            ),
            expectRestAdapterPath:
                '../test/integration-test/expect-result/rest-adapter-config.json',
            expectSchemaPath:
                '../test/integration-test/expect-result/schema.json',
            expectSyncOperationConfigurationPath:
                '../test/integration-test/expect-result/sync-operation-configuration.json',
            expectTranslation:
                '../test/integration-test/expect-result/translation.js',
            operationConfigurationFilePath: path.join(
                operationName,
                ARTIFACTS.OperationConfigurationFile
            ),
            operationFolderPath: path.join(operationName),
            requestSchemaFilePath: path.join(
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.RequestSchemaFile
            ),
            responseSchemaFilePath: path.join(
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.ResponseSchemaFile
            ),
            schemaFolderPath: path.join(operationName, ARTIFACTS.SchemasFolder),
            translationFilePath: path.join(
                operationName,
                ARTIFACTS.TranslationsFolder,
                ARTIFACTS.TranslationFile
            ),
            translationFolderPath: path.join(
                operationName,
                ARTIFACTS.TranslationsFolder
            )
        };

        test.stdout()
            .command([
                'create:operation',
                operationName,
                '--sync',
                'true',
                '-a',
                AdapterType.REST,
                '--validateRequest',
                'true',
                '--validateResponse',
                'true',
                '--httpEnabled',
                'NONE',
                '--verbOnly',
                'false'
            ])
            .it(
                'runs create:operation goodOperation --sync true -a Adapters.Rest --validateRequest true --validateResponse true',
                ctx => {
                    let actualTranslation = fs.readFileSync(
                        input.translationFilePath,
                        'utf8'
                    );

                    let expectTranslation = fs.readFileSync(
                        input.expectTranslation,
                        'utf8'
                    );

                    let actualAdapterConfigFile = fs.readFileSync(
                        input.adapterConfigFilePath,
                        'utf8'
                    );

                    let expectRestAdapter = fs.readFileSync(
                        input.expectRestAdapterPath,
                        'utf8'
                    );

                    let actualSyncOperationConfiguration = fs.readFileSync(
                        input.operationConfigurationFilePath,
                        'utf8'
                    );

                    let expectSyncOperationConfiguration = fs.readFileSync(
                        input.expectSyncOperationConfigurationPath,
                        'utf8'
                    );

                    let actualRequest = fs.readFileSync(
                        input.requestSchemaFilePath,
                        'utf8'
                    );

                    let actualResponse = fs.readFileSync(
                        input.responseSchemaFilePath,
                        'utf8'
                    );

                    let expectSchema = fs.readFileSync(
                        input.expectSchemaPath,
                        'utf8'
                    );

                    assert.isTrue(fs.existsSync(input.operationFolderPath));

                    assert.isTrue(fs.existsSync(input.translationFolderPath));

                    assert.equal(expectTranslation, actualTranslation);

                    assert.isTrue(fs.existsSync(input.configurationFolderPath));

                    assert.isTrue(fs.existsSync(input.adapterConfigFilePath));

                    assert.equal(expectRestAdapter, actualAdapterConfigFile);

                    assert.isTrue(
                        fs.existsSync(input.operationConfigurationFilePath)
                    );

                    assert.isTrue(fs.existsSync(input.schemaFolderPath));

                    assert.isTrue(fs.existsSync(input.requestSchemaFilePath));
                    assert.isTrue(fs.existsSync(input.responseSchemaFilePath));

                    assert.deepEqual(expectSchema, actualRequest);
                    assert.deepEqual(expectSchema, actualResponse);

                    assert.deepEqual(
                        expectSyncOperationConfiguration,
                        actualSyncOperationConfiguration
                    );
                }
            );
    });

    describe('create sync operation with validation request and validation response and http enable with verb GET', () => {
        const operationName = 'goodOperation';

        let input = {
            adapterConfigFilePath: path.join(
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder,
                ARTIFACTS.AdapterConfigFile
            ),
            configurationFolderPath: path.join(
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder
            ),
            expectRestAdapterPath:
                '../test/integration-test/expect-result/rest-adapter-config.json',
            expectSchemaPath:
                '../test/integration-test/expect-result/schema.json',
            expectSyncOperationWithHTTPGETConfigurationPath:
                '../test/integration-test/expect-result/sync-operation-http-get-configuration.json',
            expectTranslation:
                '../test/integration-test/expect-result/translation.js',
            operationConfigurationFilePath: path.join(
                operationName,
                ARTIFACTS.OperationConfigurationFile
            ),
            operationFolderPath: path.join(operationName),
            requestSchemaFilePath: path.join(
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.RequestSchemaFile
            ),
            responseSchemaFilePath: path.join(
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.ResponseSchemaFile
            ),
            schemaFolderPath: path.join(operationName, ARTIFACTS.SchemasFolder),
            translationFilePath: path.join(
                operationName,
                ARTIFACTS.TranslationsFolder,
                ARTIFACTS.TranslationFile
            ),
            translationFolderPath: path.join(
                operationName,
                ARTIFACTS.TranslationsFolder
            )
        };

        test.stdout()
            .command([
                'create:operation',
                operationName,
                '--sync',
                'true',
                '-a',
                AdapterType.REST,
                '--validateRequest',
                'true',
                '--validateResponse',
                'true',
                '--httpEnabled',
                'GET',
                '--verbOnly',
                'false'
            ])
            .it(
                'runs create:operation goodOperation --sync true --validateRequest true --validateResponse true',
                ctx => {
                    let actualTranslation = fs.readFileSync(
                        input.translationFilePath,
                        'utf8'
                    );

                    let expectTranslation = fs.readFileSync(
                        input.expectTranslation,
                        'utf8'
                    );

                    let actualAdapterConfigFile = fs.readFileSync(
                        input.adapterConfigFilePath,
                        'utf8'
                    );

                    let expectRestAdapter = fs.readFileSync(
                        input.expectRestAdapterPath,
                        'utf8'
                    );

                    let actualSyncOperationConfiguration = fs.readFileSync(
                        input.operationConfigurationFilePath,
                        'utf8'
                    );

                    let expectOperationConfiguration = fs.readFileSync(
                        input.expectSyncOperationWithHTTPGETConfigurationPath,
                        'utf8'
                    );

                    let actualRequest = fs.readFileSync(
                        input.requestSchemaFilePath,
                        'utf8'
                    );

                    let actualResponse = fs.readFileSync(
                        input.responseSchemaFilePath,
                        'utf8'
                    );

                    let expectSchema = fs.readFileSync(
                        input.expectSchemaPath,
                        'utf8'
                    );

                    assert.isTrue(fs.existsSync(input.operationFolderPath));

                    assert.isTrue(fs.existsSync(input.translationFolderPath));

                    assert.equal(expectTranslation, actualTranslation);

                    assert.isTrue(fs.existsSync(input.configurationFolderPath));

                    assert.isTrue(fs.existsSync(input.adapterConfigFilePath));

                    assert.equal(expectRestAdapter, actualAdapterConfigFile);

                    assert.isTrue(
                        fs.existsSync(input.operationConfigurationFilePath)
                    );

                    assert.isTrue(fs.existsSync(input.schemaFolderPath));

                    assert.isTrue(fs.existsSync(input.requestSchemaFilePath));
                    assert.isTrue(fs.existsSync(input.responseSchemaFilePath));

                    assert.deepEqual(expectSchema, actualRequest);
                    assert.deepEqual(expectSchema, actualResponse);

                    assert.deepEqual(
                        expectOperationConfiguration,
                        actualSyncOperationConfiguration
                    );
                }
            );
    });

    describe('create sync operation with validation request and validation response with rest adapter, and http enable with verb GET, and verbOnly with true', () => {
        const operationName = 'goodOperation';

        let input = {
            adapterConfigFilePath: path.join(
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder,
                ARTIFACTS.AdapterConfigFile
            ),
            configurationFolderPath: path.join(
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder
            ),
            expectRestAdapterPath:
                '../test/integration-test/expect-result/rest-adapter-config.json',
            expectSchemaPath:
                '../test/integration-test/expect-result/schema.json',
            expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath:
                '../test/integration-test/expect-result/sync-operation-http-get-configuration-verb-only.json',
            expectTranslation:
                '../test/integration-test/expect-result/translation.js',
            operationConfigurationFilePath: path.join(
                operationName,
                ARTIFACTS.OperationConfigurationFile
            ),
            operationFolderPath: path.join(operationName),
            requestSchemaFilePath: path.join(
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.RequestSchemaFile
            ),
            responseSchemaFilePath: path.join(
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.ResponseSchemaFile
            ),
            schemaFolderPath: path.join(operationName, ARTIFACTS.SchemasFolder),
            translationFilePath: path.join(
                operationName,
                ARTIFACTS.TranslationsFolder,
                ARTIFACTS.TranslationFile
            ),
            translationFolderPath: path.join(
                operationName,
                ARTIFACTS.TranslationsFolder
            )
        };

        test.stdout()
            .command([
                'create:operation',
                operationName,
                '--sync',
                'true',
                '-a',
                AdapterType.REST,
                '--validateRequest',
                'true',
                '--validateResponse',
                'true',
                '--httpEnabled',
                'GET',
                '--verbOnly',
                'true'
            ])
            .it(
                'runs create:operation goodOperation --sync true -a Adapters.Rest --validateRequest true --validateResponse true --httpEnabled GET --verbOnly true',
                ctx => {
                    let actualTranslation = fs.readFileSync(
                        input.translationFilePath,
                        'utf8'
                    );

                    let expectTranslation = fs.readFileSync(
                        input.expectTranslation,
                        'utf8'
                    );

                    let actualAdapterConfigFile = fs.readFileSync(
                        input.adapterConfigFilePath,
                        'utf8'
                    );

                    let expectRestAdapter = fs.readFileSync(
                        input.expectRestAdapterPath,
                        'utf8'
                    );

                    let actualSyncOperationConfiguration = fs.readFileSync(
                        input.operationConfigurationFilePath,
                        'utf8'
                    );

                    let expectOperationConfiguration = fs.readFileSync(
                        input.expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath,
                        'utf8'
                    );

                    let actualRequest = fs.readFileSync(
                        input.requestSchemaFilePath,
                        'utf8'
                    );

                    let actualResponse = fs.readFileSync(
                        input.responseSchemaFilePath,
                        'utf8'
                    );

                    let expectSchema = fs.readFileSync(
                        input.expectSchemaPath,
                        'utf8'
                    );

                    assert.isTrue(fs.existsSync(input.operationFolderPath));

                    assert.isTrue(fs.existsSync(input.translationFolderPath));

                    assert.equal(expectTranslation, actualTranslation);

                    assert.isTrue(fs.existsSync(input.configurationFolderPath));

                    assert.isTrue(fs.existsSync(input.adapterConfigFilePath));

                    assert.equal(expectRestAdapter, actualAdapterConfigFile);

                    assert.isTrue(
                        fs.existsSync(input.operationConfigurationFilePath)
                    );

                    assert.isTrue(fs.existsSync(input.schemaFolderPath));

                    assert.isTrue(fs.existsSync(input.requestSchemaFilePath));
                    assert.isTrue(fs.existsSync(input.responseSchemaFilePath));

                    assert.deepEqual(expectSchema, actualRequest);
                    assert.deepEqual(expectSchema, actualResponse);

                    assert.deepEqual(
                        expectOperationConfiguration,
                        actualSyncOperationConfiguration
                    );
                }
            );
    });

    describe(
        'create sync operation with validation request and validation response true, and http ' +
            'enabled with verb GET, and verbOnly with true with database adapter',
        () => {
            const operationName = 'goodOperation';

            let input = {
                adapterConfigFilePath: path.join(
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder,
                    ARTIFACTS.AdapterConfigFile
                ),
                configurationFolderPath: path.join(
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder
                ),
                expectDatabaseAdapterPath:
                    '../test/integration-test/expect-result/database-adapter-config.json',
                expectSchemaPath:
                    '../test/integration-test/expect-result/schema.json',
                expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath:
                    '../test/integration-test/expect-result/sync-operation-http-get-configuration-verb-only.json',
                expectTranslation:
                    '../test/integration-test/expect-result/translation.js',
                operationConfigurationFilePath: path.join(
                    operationName,
                    ARTIFACTS.OperationConfigurationFile
                ),
                operationFolderPath: path.join(operationName),
                requestSchemaFilePath: path.join(
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.RequestSchemaFile
                ),
                responseSchemaFilePath: path.join(
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.ResponseSchemaFile
                ),
                schemaFolderPath: path.join(
                    operationName,
                    ARTIFACTS.SchemasFolder
                ),
                translationFilePath: path.join(
                    operationName,
                    ARTIFACTS.TranslationsFolder,
                    ARTIFACTS.TranslationFile
                ),
                translationFolderPath: path.join(
                    operationName,
                    ARTIFACTS.TranslationsFolder
                )
            };

            test.stdout()
                .command([
                    'create:operation',
                    operationName,
                    '--sync',
                    'true',
                    '-a',
                    AdapterType.DATABASE,
                    '--validateRequest',
                    'true',
                    '--validateResponse',
                    'true',
                    '--httpEnabled',
                    'GET',
                    '--verbOnly',
                    'true'
                ])
                .it(
                    'runs create:operation goodOperation --sync true -a Adapters.Database --validateRequest true --validateResponse true --httpEnabled GET --verbOnly true',
                    ctx => {
                        let actualTranslation = fs.readFileSync(
                            input.translationFilePath,
                            'utf8'
                        );

                        let expectTranslation = fs.readFileSync(
                            input.expectTranslation,
                            'utf8'
                        );

                        let actualAdapterConfigFile = fs.readFileSync(
                            input.adapterConfigFilePath,
                            'utf8'
                        );

                        let expectDatabaseAdapter = fs.readFileSync(
                            input.expectDatabaseAdapterPath,
                            'utf8'
                        );

                        let actualSyncOperationConfiguration = fs.readFileSync(
                            input.operationConfigurationFilePath,
                            'utf8'
                        );

                        let expectOperationConfiguration = fs.readFileSync(
                            input.expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath,
                            'utf8'
                        );

                        let actualRequest = fs.readFileSync(
                            input.requestSchemaFilePath,
                            'utf8'
                        );

                        let actualResponse = fs.readFileSync(
                            input.responseSchemaFilePath,
                            'utf8'
                        );

                        let expectSchema = fs.readFileSync(
                            input.expectSchemaPath,
                            'utf8'
                        );

                        assert.isTrue(fs.existsSync(input.operationFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.translationFolderPath)
                        );

                        assert.equal(expectTranslation, actualTranslation);

                        assert.isTrue(
                            fs.existsSync(input.configurationFolderPath)
                        );

                        assert.isTrue(
                            fs.existsSync(input.adapterConfigFilePath)
                        );

                        assert.equal(
                            expectDatabaseAdapter,
                            actualAdapterConfigFile
                        );

                        assert.isTrue(
                            fs.existsSync(input.operationConfigurationFilePath)
                        );

                        assert.isTrue(fs.existsSync(input.schemaFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.requestSchemaFilePath)
                        );
                        assert.isTrue(
                            fs.existsSync(input.responseSchemaFilePath)
                        );

                        assert.deepEqual(expectSchema, actualRequest);
                        assert.deepEqual(expectSchema, actualResponse);

                        assert.deepEqual(
                            expectOperationConfiguration,
                            actualSyncOperationConfiguration
                        );
                    }
                );
        }
    );

    describe(
        'create sync operation with validation request and validation response true, and http ' +
            'enabled with verb GET, and verbOnly with true with mock adapter',
        () => {
            const operationName = 'goodOperation';

            let input = {
                adapterConfigFilePath: path.join(
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder,
                    ARTIFACTS.AdapterConfigFile
                ),
                configurationFolderPath: path.join(
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder
                ),
                expectMockAdapterPath:
                    '../test/integration-test/expect-result/mock-adapter-config.json',
                expectSchemaPath:
                    '../test/integration-test/expect-result/schema.json',
                expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath:
                    '../test/integration-test/expect-result/sync-operation-http-get-configuration-verb-only.json',
                expectTranslation:
                    '../test/integration-test/expect-result/translation.js',
                operationConfigurationFilePath: path.join(
                    operationName,
                    ARTIFACTS.OperationConfigurationFile
                ),
                operationFolderPath: path.join(operationName),
                requestSchemaFilePath: path.join(
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.RequestSchemaFile
                ),
                responseSchemaFilePath: path.join(
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.ResponseSchemaFile
                ),
                schemaFolderPath: path.join(
                    operationName,
                    ARTIFACTS.SchemasFolder
                ),
                translationFilePath: path.join(
                    operationName,
                    ARTIFACTS.TranslationsFolder,
                    ARTIFACTS.TranslationFile
                ),
                translationFolderPath: path.join(
                    operationName,
                    ARTIFACTS.TranslationsFolder
                )
            };

            test.stdout()
                .command([
                    'create:operation',
                    operationName,
                    '--sync',
                    'true',
                    '-a',
                    AdapterType.MOCK,
                    '--validateRequest',
                    'true',
                    '--validateResponse',
                    'true',
                    '--httpEnabled',
                    'GET',
                    '--verbOnly',
                    'true'
                ])
                .it(
                    'runs create:operation goodOperation --sync true -a Adapters.Mock --validateRequest true --validateResponse true --verbOnly true',
                    ctx => {
                        let actualTranslation = fs.readFileSync(
                            input.translationFilePath,
                            'utf8'
                        );

                        let expectTranslation = fs.readFileSync(
                            input.expectTranslation,
                            'utf8'
                        );

                        let actualAdapterConfigFile = fs.readFileSync(
                            input.adapterConfigFilePath,
                            'utf8'
                        );

                        let expectMockAdapter = fs.readFileSync(
                            input.expectMockAdapterPath,
                            'utf8'
                        );

                        let actualSyncOperationConfiguration = fs.readFileSync(
                            input.operationConfigurationFilePath,
                            'utf8'
                        );

                        let expectOperationConfiguration = fs.readFileSync(
                            input.expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath,
                            'utf8'
                        );

                        let actualRequest = fs.readFileSync(
                            input.requestSchemaFilePath,
                            'utf8'
                        );

                        let actualResponse = fs.readFileSync(
                            input.responseSchemaFilePath,
                            'utf8'
                        );

                        let expectSchema = fs.readFileSync(
                            input.expectSchemaPath,
                            'utf8'
                        );

                        assert.isTrue(fs.existsSync(input.operationFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.translationFolderPath)
                        );

                        assert.equal(expectTranslation, actualTranslation);

                        assert.isTrue(
                            fs.existsSync(input.configurationFolderPath)
                        );

                        assert.isTrue(
                            fs.existsSync(input.adapterConfigFilePath)
                        );

                        assert.equal(
                            expectMockAdapter,
                            actualAdapterConfigFile
                        );

                        assert.isTrue(
                            fs.existsSync(input.operationConfigurationFilePath)
                        );

                        assert.isTrue(fs.existsSync(input.schemaFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.requestSchemaFilePath)
                        );
                        assert.isTrue(
                            fs.existsSync(input.responseSchemaFilePath)
                        );

                        assert.deepEqual(expectSchema, actualRequest);
                        assert.deepEqual(expectSchema, actualResponse);

                        assert.deepEqual(
                            expectOperationConfiguration,
                            actualSyncOperationConfiguration
                        );
                    }
                );
        }
    );
});

const deleteFolderRecursive = function(inputPath: string) {
    let files: string[] = [];
    if (fs.existsSync(inputPath)) {
        files = fs.readdirSync(inputPath);
        files.forEach(function(file, index) {
            let curPath = inputPath + '/' + file;
            if (fs.lstatSync(curPath).isDirectory()) {
                deleteFolderRecursive(curPath);
            } else {
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(inputPath);
    }
};

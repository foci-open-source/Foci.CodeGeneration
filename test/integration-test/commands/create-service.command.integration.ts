import { expect, test } from '@oclif/test';
import * as fs from 'fs';
import * as path from 'path';
import { assert } from 'chai';
import ARTIFACTS from '../../../src/constants/artifacts';
import { AdapterType } from '../../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';

describe('Create service command', () => {
    const serviceName = 'ServeGoodman';

    afterEach(() => {
        deleteFolderRecursive(serviceName);
    });

    describe('create service without any flag', () => {
        let input = {
            expectOrbitalPath:
                './test/integration-test/expect-result/orbital.json',
            orbitalFilePath: path.join(serviceName, ARTIFACTS.OrbitalFile)
        };
        test.stdout()
            .command(['create:service', serviceName])
            .it('runs create:service ServeGoodman', ctx => {
                expect(ctx.stdout).to.contain(
                    `Service: ${serviceName} was created successfully`
                );

                assert.isTrue(fs.existsSync(serviceName));
                assert.isTrue(fs.existsSync(input.orbitalFilePath));

                let actual = fs.readFileSync(input.orbitalFilePath, 'utf8');
                let expectOrbital = fs.readFileSync(
                    input.expectOrbitalPath,
                    'utf8'
                );
                assert.equal(expectOrbital, actual);
            });
    });

    describe('create service with async operation with rest adapter', () => {
        const operationName = 'goodOperation';
        let input = {
            adapterConfigFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder,
                ARTIFACTS.AdapterConfigFile
            ),
            configurationFolderPath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder
            ),
            expectDefaultOperationConfigurationPath:
                './test/integration-test/expect-result/async-operation-configuration.json',
            expectRestAdapterPath:
                './test/integration-test/expect-result/rest-adapter-config.json',
            expectSchemaPath:
                './test/integration-test/expect-result/schema.json',
            expectTranslation:
                './test/integration-test/expect-result/translation.js',
            operationConfigurationFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.OperationConfigurationFile
            ),
            operationFolderPath: path.join(serviceName, operationName),
            translationFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.TranslationsFolder,
                ARTIFACTS.TranslationFile
            ),
            translationFolderPath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.TranslationsFolder
            )
        };

        test.stdout()
            .command([
                'create:service',
                serviceName,
                '-o',
                operationName,
                '-a',
                AdapterType.REST,
                '-s',
                'false',
                '--validateRequest',
                'false',
                '--validateResponse',
                'false',
                '--httpEnabled',
                'NONE',
                '--verbOnly',
                'false'
            ])
            .it(
                'runs create:service ServeGoodman -o goodOperation -a Adapters.Rest -s false --validateRequest false --validateResponse false --httpEnabled NONE --verbOnly false',
                ctx => {
                    let actualTranslation = fs.readFileSync(
                        input.translationFilePath,
                        'utf8'
                    );

                    let expectTranslation = fs.readFileSync(
                        input.expectTranslation,
                        'utf8'
                    );

                    let actualAdapterConfigFile = fs.readFileSync(
                        input.adapterConfigFilePath,
                        'utf8'
                    );

                    let expectRestAdapter = fs.readFileSync(
                        input.expectRestAdapterPath,
                        'utf8'
                    );

                    let actualDefaultOperationConfiguration = fs.readFileSync(
                        input.operationConfigurationFilePath,
                        'utf8'
                    );

                    let expectDefaultOperationConfiguration = fs.readFileSync(
                        input.expectDefaultOperationConfigurationPath,
                        'utf8'
                    );

                    assert.isTrue(fs.existsSync(input.operationFolderPath));

                    assert.isTrue(fs.existsSync(input.translationFolderPath));

                    assert.equal(expectTranslation, actualTranslation);

                    assert.isTrue(fs.existsSync(input.configurationFolderPath));

                    assert.isTrue(fs.existsSync(input.adapterConfigFilePath));

                    assert.equal(expectRestAdapter, actualAdapterConfigFile);

                    assert.isTrue(
                        fs.existsSync(input.operationConfigurationFilePath)
                    );

                    assert.deepEqual(
                        expectDefaultOperationConfiguration,
                        actualDefaultOperationConfiguration
                    );

                    expect(ctx.stdout).to.contain(
                        `Operation: ${operationName} was created successfully and added to ${serviceName}`
                    );
                }
            );
    });

    describe('create service with -o flag and operation name and sync flag with validation request and validation response true and httpEnabled None', () => {
        const operationName = 'goodOperation';
        let input = {
            adapterConfigFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder,
                ARTIFACTS.AdapterConfigFile
            ),
            configurationFolderPath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder
            ),
            expectRestAdapterPath:
                './test/integration-test/expect-result/rest-adapter-config.json',
            expectSchemaPath:
                './test/integration-test/expect-result/schema.json',
            expectSyncOperationConfigurationPath:
                './test/integration-test/expect-result/sync-operation-configuration.json',
            expectTranslation:
                './test/integration-test/expect-result/translation.js',
            operationConfigurationFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.OperationConfigurationFile
            ),
            operationFolderPath: path.join(serviceName, operationName),
            requestSchemaFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.RequestSchemaFile
            ),
            responseSchemaFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.ResponseSchemaFile
            ),
            schemaFolderPath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.SchemasFolder
            ),
            translationFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.TranslationsFolder,
                ARTIFACTS.TranslationFile
            ),
            translationFolderPath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.TranslationsFolder
            )
        };

        test.stdout()
            .command([
                'create:service',
                serviceName,
                '-o',
                operationName,
                '--sync',
                'true',
                '-a',
                AdapterType.REST,
                '--validateRequest',
                'true',
                '--validateResponse',
                'true',
                '--httpEnabled',
                'NONE',
                '--verbOnly',
                'false'
            ])
            .it(
                'runs create:service ServeGoodman -o goodOperation --sync true -a Adapters.Rest --validateRequest true --validateResponse true --httpEnabled NONE --verbOnly false',
                ctx => {
                    let actualTranslation = fs.readFileSync(
                        input.translationFilePath,
                        'utf8'
                    );

                    let expectTranslation = fs.readFileSync(
                        input.expectTranslation,
                        'utf8'
                    );

                    let actualAdapterConfigFile = fs.readFileSync(
                        input.adapterConfigFilePath,
                        'utf8'
                    );

                    let expectRestAdapter = fs.readFileSync(
                        input.expectRestAdapterPath,
                        'utf8'
                    );

                    let actualSyncOperationConfiguration = fs.readFileSync(
                        input.operationConfigurationFilePath,
                        'utf8'
                    );

                    let expectSyncOperationConfiguration = fs.readFileSync(
                        input.expectSyncOperationConfigurationPath,
                        'utf8'
                    );

                    let actualRequest = fs.readFileSync(
                        input.requestSchemaFilePath,
                        'utf8'
                    );

                    let actualResponse = fs.readFileSync(
                        input.responseSchemaFilePath,
                        'utf8'
                    );

                    let expectSchema = fs.readFileSync(
                        input.expectSchemaPath,
                        'utf8'
                    );

                    assert.equal(expectTranslation, actualTranslation);

                    assert.isTrue(fs.existsSync(input.configurationFolderPath));

                    assert.isTrue(fs.existsSync(input.adapterConfigFilePath));

                    assert.isTrue(fs.existsSync(input.operationFolderPath));

                    assert.isTrue(fs.existsSync(input.translationFolderPath));

                    assert.equal(expectRestAdapter, actualAdapterConfigFile);

                    assert.isTrue(
                        fs.existsSync(input.operationConfigurationFilePath)
                    );

                    assert.isTrue(fs.existsSync(input.schemaFolderPath));

                    assert.isTrue(fs.existsSync(input.requestSchemaFilePath));
                    assert.isTrue(fs.existsSync(input.responseSchemaFilePath));

                    assert.deepEqual(expectSchema, actualRequest);
                    assert.deepEqual(expectSchema, actualResponse);

                    assert.deepEqual(
                        expectSyncOperationConfiguration,
                        actualSyncOperationConfiguration
                    );

                    expect(ctx.stdout).to.contain(
                        `Operation: ${operationName} was created successfully and added to ${serviceName}`
                    );
                }
            );
    });

    describe('create service with sync operation and rest adapter with validation request and validation response true, and http enable with verb GET', () => {
        const operationName = 'goodOperation';
        let input = {
            adapterConfigFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder,
                ARTIFACTS.AdapterConfigFile
            ),
            configurationFolderPath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.AdapterConfigurationsFolder
            ),
            expectRestAdapterPath:
                './test/integration-test/expect-result/rest-adapter-config.json',
            expectSchemaPath:
                './test/integration-test/expect-result/schema.json',
            expectSyncOperationWithHTTPGETConfigurationPath:
                './test/integration-test/expect-result/sync-operation-http-get-configuration.json',
            expectTranslation:
                './test/integration-test/expect-result/translation.js',
            operationConfigurationFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.OperationConfigurationFile
            ),
            operationFolderPath: path.join(serviceName, operationName),
            requestSchemaFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.RequestSchemaFile
            ),
            responseSchemaFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.SchemasFolder,
                ARTIFACTS.ResponseSchemaFile
            ),
            schemaFolderPath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.SchemasFolder
            ),
            translationFilePath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.TranslationsFolder,
                ARTIFACTS.TranslationFile
            ),
            translationFolderPath: path.join(
                serviceName,
                operationName,
                ARTIFACTS.TranslationsFolder
            )
        };

        test.stdout()
            .command([
                'create:service',
                serviceName,
                '-o',
                operationName,
                '--sync',
                'true',
                '-a',
                AdapterType.REST,
                '--validateRequest',
                'true',
                '--validateResponse',
                'true',
                '--httpEnabled',
                'GET',
                '--verbOnly',
                'false'
            ])
            .it(
                'runs create:service ServeGoodman -o goodOperation --sync true -a Adapters.Rest --validateRequest true --validateResponse true --httpEnabled GET --verbOnly false',
                ctx => {
                    let actualTranslation = fs.readFileSync(
                        input.translationFilePath,
                        'utf8'
                    );

                    let expectTranslation = fs.readFileSync(
                        input.expectTranslation,
                        'utf8'
                    );

                    let actualAdapterConfigFile = fs.readFileSync(
                        input.adapterConfigFilePath,
                        'utf8'
                    );

                    let expectRestAdapter = fs.readFileSync(
                        input.expectRestAdapterPath,
                        'utf8'
                    );

                    let actualOperationConfiguration = fs.readFileSync(
                        input.operationConfigurationFilePath,
                        'utf8'
                    );

                    let expectOperationConfiguration = fs.readFileSync(
                        input.expectSyncOperationWithHTTPGETConfigurationPath,
                        'utf8'
                    );

                    let actualRequest = fs.readFileSync(
                        input.requestSchemaFilePath,
                        'utf8'
                    );

                    let actualResponse = fs.readFileSync(
                        input.responseSchemaFilePath,
                        'utf8'
                    );

                    let expectSchema = fs.readFileSync(
                        input.expectSchemaPath,
                        'utf8'
                    );

                    assert.equal(expectTranslation, actualTranslation);

                    assert.isTrue(fs.existsSync(input.configurationFolderPath));

                    assert.isTrue(fs.existsSync(input.adapterConfigFilePath));

                    assert.isTrue(fs.existsSync(input.operationFolderPath));

                    assert.isTrue(fs.existsSync(input.translationFolderPath));

                    assert.equal(expectRestAdapter, actualAdapterConfigFile);

                    assert.isTrue(
                        fs.existsSync(input.operationConfigurationFilePath)
                    );

                    assert.isTrue(fs.existsSync(input.schemaFolderPath));

                    assert.isTrue(fs.existsSync(input.requestSchemaFilePath));
                    assert.isTrue(fs.existsSync(input.responseSchemaFilePath));

                    assert.deepEqual(expectSchema, actualRequest);
                    assert.deepEqual(expectSchema, actualResponse);

                    assert.deepEqual(
                        expectOperationConfiguration,
                        actualOperationConfiguration
                    );

                    expect(ctx.stdout).to.contain(
                        `Operation: ${operationName} was created successfully and added to ${serviceName}`
                    );
                }
            );
    });

    describe(
        'create service with sync operation and rest adapter with validation request and validation response true, and http enable with verb GET, and verbOnly with true',
        () => {
            const operationName = 'goodOperation';
            let input = {
                adapterConfigFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder,
                    ARTIFACTS.AdapterConfigFile
                ),
                configurationFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder
                ),
                expectRestAdapterPath:
                    './test/integration-test/expect-result/rest-adapter-config.json',
                expectSchemaPath:
                    './test/integration-test/expect-result/schema.json',
                expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath:
                    './test/integration-test/expect-result/sync-operation-http-get-configuration-verb-only.json',
                expectTranslation:
                    './test/integration-test/expect-result/translation.js',
                operationConfigurationFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.OperationConfigurationFile
                ),
                operationFolderPath: path.join(serviceName, operationName),
                requestSchemaFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.RequestSchemaFile
                ),
                responseSchemaFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.ResponseSchemaFile
                ),
                schemaFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder
                ),
                translationFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.TranslationsFolder,
                    ARTIFACTS.TranslationFile
                ),
                translationFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.TranslationsFolder
                )
            };

            test.stdout()
                .command([
                    'create:service',
                    serviceName,
                    '-o',
                    operationName,
                    '--sync',
                    'true',
                    '-a',
                    AdapterType.REST,
                    '--validateRequest',
                    'true',
                    '--validateResponse',
                    'true',
                    '--httpEnabled',
                    'GET',
                    '--verbOnly',
                    'true'
                ])
                .it(
                    'runs create:service ServeGoodman -o goodOperation --sync true --validateRequest true --validateResponse true --httpEnabled GET --verbOnly true',
                    ctx => {
                        let actualTranslation = fs.readFileSync(
                            input.translationFilePath,
                            'utf8'
                        );

                        let expectTranslation = fs.readFileSync(
                            input.expectTranslation,
                            'utf8'
                        );

                        let actualAdapterConfigFile = fs.readFileSync(
                            input.adapterConfigFilePath,
                            'utf8'
                        );

                        let expectRestAdapter = fs.readFileSync(
                            input.expectRestAdapterPath,
                            'utf8'
                        );

                        let actualOperationConfiguration = fs.readFileSync(
                            input.operationConfigurationFilePath,
                            'utf8'
                        );

                        let expectOperationConfiguration = fs.readFileSync(
                            input.expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath,
                            'utf8'
                        );

                        let actualRequest = fs.readFileSync(
                            input.requestSchemaFilePath,
                            'utf8'
                        );

                        let actualResponse = fs.readFileSync(
                            input.responseSchemaFilePath,
                            'utf8'
                        );

                        let expectSchema = fs.readFileSync(
                            input.expectSchemaPath,
                            'utf8'
                        );

                        assert.equal(expectTranslation, actualTranslation);

                        assert.isTrue(
                            fs.existsSync(input.configurationFolderPath)
                        );

                        assert.isTrue(
                            fs.existsSync(input.adapterConfigFilePath)
                        );

                        assert.isTrue(fs.existsSync(input.operationFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.translationFolderPath)
                        );

                        assert.equal(
                            expectRestAdapter,
                            actualAdapterConfigFile
                        );

                        assert.isTrue(
                            fs.existsSync(input.operationConfigurationFilePath)
                        );

                        assert.isTrue(fs.existsSync(input.schemaFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.requestSchemaFilePath)
                        );
                        assert.isTrue(
                            fs.existsSync(input.responseSchemaFilePath)
                        );

                        assert.deepEqual(expectSchema, actualRequest);
                        assert.deepEqual(expectSchema, actualResponse);

                        assert.deepEqual(
                            expectOperationConfiguration,
                            actualOperationConfiguration
                        );

                        expect(ctx.stdout).to.contain(
                            `Operation: ${operationName} was created successfully and added to ${serviceName}`
                        );
                    }
                );
        }
    );

    describe(
        'create service with sync operation with validation request and validation response true, and http enable with verb GET, and verbOnly with true',
        () => {
            const operationName = 'goodOperation';
            let input = {
                adapterConfigFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder,
                    ARTIFACTS.AdapterConfigFile
                ),
                configurationFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder
                ),
                expectRestAdapterPath:
                    './test/integration-test/expect-result/rest-adapter-config.json',
                expectSchemaPath:
                    './test/integration-test/expect-result/schema.json',
                expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath:
                    './test/integration-test/expect-result/sync-operation-http-get-configuration-verb-only.json',
                expectTranslation:
                    './test/integration-test/expect-result/translation.js',
                operationConfigurationFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.OperationConfigurationFile
                ),
                operationFolderPath: path.join(serviceName, operationName),
                requestSchemaFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.RequestSchemaFile
                ),
                responseSchemaFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.ResponseSchemaFile
                ),
                schemaFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder
                ),
                translationFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.TranslationsFolder,
                    ARTIFACTS.TranslationFile
                ),
                translationFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.TranslationsFolder
                )
            };

            test.stdout()
                .command([
                    'create:service',
                    serviceName,
                    '-o',
                    operationName,
                    '--sync',
                    'true',
                    '-a',
                    AdapterType.REST,
                    '--validateRequest',
                    'true',
                    '--validateResponse',
                    'true',
                    '--httpEnabled',
                    'GET',
                    '--verbOnly',
                    'true'
                ])
                .it(
                    'runs create:service ServeGoodman -o goodOperation --sync true --validateRequest true --validateResponse true --httpEnabled GET --verbOnly true',
                    ctx => {
                        let actualTranslation = fs.readFileSync(
                            input.translationFilePath,
                            'utf8'
                        );

                        let expectTranslation = fs.readFileSync(
                            input.expectTranslation,
                            'utf8'
                        );

                        let actualAdapterConfigFile = fs.readFileSync(
                            input.adapterConfigFilePath,
                            'utf8'
                        );

                        let expectRestAdapter = fs.readFileSync(
                            input.expectRestAdapterPath,
                            'utf8'
                        );

                        let actualOperationConfiguration = fs.readFileSync(
                            input.operationConfigurationFilePath,
                            'utf8'
                        );

                        let expectOperationConfiguration = fs.readFileSync(
                            input.expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath,
                            'utf8'
                        );

                        let actualRequest = fs.readFileSync(
                            input.requestSchemaFilePath,
                            'utf8'
                        );

                        let actualResponse = fs.readFileSync(
                            input.responseSchemaFilePath,
                            'utf8'
                        );

                        let expectSchema = fs.readFileSync(
                            input.expectSchemaPath,
                            'utf8'
                        );

                        assert.equal(expectTranslation, actualTranslation);

                        assert.isTrue(
                            fs.existsSync(input.configurationFolderPath)
                        );

                        assert.isTrue(
                            fs.existsSync(input.adapterConfigFilePath)
                        );

                        assert.isTrue(fs.existsSync(input.operationFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.translationFolderPath)
                        );

                        assert.equal(
                            expectRestAdapter,
                            actualAdapterConfigFile
                        );

                        assert.isTrue(
                            fs.existsSync(input.operationConfigurationFilePath)
                        );

                        assert.isTrue(fs.existsSync(input.schemaFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.requestSchemaFilePath)
                        );
                        assert.isTrue(
                            fs.existsSync(input.responseSchemaFilePath)
                        );

                        assert.deepEqual(expectSchema, actualRequest);
                        assert.deepEqual(expectSchema, actualResponse);

                        assert.deepEqual(
                            expectOperationConfiguration,
                            actualOperationConfiguration
                        );

                        expect(ctx.stdout).to.contain(
                            `Operation: ${operationName} was created successfully and added to ${serviceName}`
                        );
                    }
                );
        }
    );

	describe(
        'create service sync operation with validation request and' +
            'validation response true, and http enable with verb GET, and verbOnly with true with database adapter',
        () => {
            const operationName = 'goodOperation';
            let input = {
                adapterConfigFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder,
                    ARTIFACTS.AdapterConfigFile
                ),
                configurationFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder
                ),
                expectDatabaseAdapterPath:
                    './test/integration-test/expect-result/database-adapter-config.json',
                expectSchemaPath:
                    './test/integration-test/expect-result/schema.json',
                expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath:
                    './test/integration-test/expect-result/sync-operation-http-get-configuration-verb-only.json',
                expectTranslation:
                    './test/integration-test/expect-result/translation.js',
                operationConfigurationFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.OperationConfigurationFile
                ),
                operationFolderPath: path.join(serviceName, operationName),
                requestSchemaFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.RequestSchemaFile
                ),
                responseSchemaFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.ResponseSchemaFile
                ),
                schemaFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder
                ),
                translationFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.TranslationsFolder,
                    ARTIFACTS.TranslationFile
                ),
                translationFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.TranslationsFolder
                )
            };

            test.stdout()
                .command([
                    'create:service',
                    serviceName,
                    '-o',
                    operationName,
                    '--sync',
                    'true',
                    '-a',
                    AdapterType.DATABASE,
                    '--validateRequest',
                    'true',
                    '--validateResponse',
                    'true',
                    '--httpEnabled',
                    'GET',
                    '--verbOnly',
                    'true'
                ])
                .it(
                    'runs create:service ServeGoodman -o goodOperation --sync true --validateRequest true --validateResponse true --httpEnabled GET --verbOnly true',
                    ctx => {
                        let actualTranslation = fs.readFileSync(
                            input.translationFilePath,
                            'utf8'
                        );

                        let expectTranslation = fs.readFileSync(
                            input.expectTranslation,
                            'utf8'
                        );

                        let actualAdapterConfigFile = fs.readFileSync(
                            input.adapterConfigFilePath,
                            'utf8'
                        );

                        let expectDatabaseAdapter = fs.readFileSync(
                            input.expectDatabaseAdapterPath,
                            'utf8'
                        );

                        let actualOperationConfiguration = fs.readFileSync(
                            input.operationConfigurationFilePath,
                            'utf8'
                        );

                        let expectOperationConfiguration = fs.readFileSync(
                            input.expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath,
                            'utf8'
                        );

                        let actualRequest = fs.readFileSync(
                            input.requestSchemaFilePath,
                            'utf8'
                        );

                        let actualResponse = fs.readFileSync(
                            input.responseSchemaFilePath,
                            'utf8'
                        );

                        let expectSchema = fs.readFileSync(
                            input.expectSchemaPath,
                            'utf8'
                        );

                        assert.equal(expectTranslation, actualTranslation);

                        assert.isTrue(
                            fs.existsSync(input.configurationFolderPath)
                        );

                        assert.isTrue(
                            fs.existsSync(input.adapterConfigFilePath)
                        );

                        assert.isTrue(fs.existsSync(input.operationFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.translationFolderPath)
                        );

                        assert.equal(
                            expectDatabaseAdapter,
                            actualAdapterConfigFile
                        );

                        assert.isTrue(
                            fs.existsSync(input.operationConfigurationFilePath)
                        );

                        assert.isTrue(fs.existsSync(input.schemaFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.requestSchemaFilePath)
                        );
                        assert.isTrue(
                            fs.existsSync(input.responseSchemaFilePath)
                        );

                        assert.deepEqual(expectSchema, actualRequest);
                        assert.deepEqual(expectSchema, actualResponse);

                        assert.deepEqual(
                            expectOperationConfiguration,
                            actualOperationConfiguration
                        );

                        expect(ctx.stdout).to.contain(
                            `Operation: ${operationName} was created successfully and added to ${serviceName}`
                        );
                    }
                );
        }
    );

    describe(
        'create service sync operation with validation request and' +
            'validation response true, and http enable with verb GET, and verbOnly with true with mock adapter',
        () => {
            const operationName = 'goodOperation';
            let input = {
                adapterConfigFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder,
                    ARTIFACTS.AdapterConfigFile
                ),
                configurationFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.AdapterConfigurationsFolder
                ),
                expectMockAdapterPath:
                    './test/integration-test/expect-result/mock-adapter-config.json',
                expectSchemaPath:
                    './test/integration-test/expect-result/schema.json',
                expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath:
                    './test/integration-test/expect-result/sync-operation-http-get-configuration-verb-only.json',
                expectTranslation:
                    './test/integration-test/expect-result/translation.js',
                operationConfigurationFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.OperationConfigurationFile
                ),
                operationFolderPath: path.join(serviceName, operationName),
                requestSchemaFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.RequestSchemaFile
                ),
                responseSchemaFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder,
                    ARTIFACTS.ResponseSchemaFile
                ),
                schemaFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.SchemasFolder
                ),
                translationFilePath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.TranslationsFolder,
                    ARTIFACTS.TranslationFile
                ),
                translationFolderPath: path.join(
                    serviceName,
                    operationName,
                    ARTIFACTS.TranslationsFolder
                )
            };

            test.stdout()
                .command([
                    'create:service',
                    serviceName,
                    '-o',
                    operationName,
                    '--sync',
                    'true',
                    '-a',
                    AdapterType.MOCK,
                    '--validateRequest',
                    'true',
                    '--validateResponse',
                    'true',
                    '--httpEnabled',
                    'GET',
                    '--verbOnly',
                    'true'
                ])
                .it(
                    'runs create:service ServeGoodman -o goodOperation --sync true -a Adapters.Mock ' +
                    '--validateRequest true --validateResponse true --httpEnabled GET --verbOnly true',
                    ctx => {
                        let actualTranslation = fs.readFileSync(
                            input.translationFilePath,
                            'utf8'
                        );

                        let expectTranslation = fs.readFileSync(
                            input.expectTranslation,
                            'utf8'
                        );

                        let actualAdapterConfigFile = fs.readFileSync(
                            input.adapterConfigFilePath,
                            'utf8'
                        );

                        let expectMockAdapter = fs.readFileSync(
                            input.expectMockAdapterPath,
                            'utf8'
                        );

                        let actualOperationConfiguration = fs.readFileSync(
                            input.operationConfigurationFilePath,
                            'utf8'
                        );

                        let expectOperationConfiguration = fs.readFileSync(
                            input.expectSyncOperationWithHTTPGETVerbOnlyConfigurationPath,
                            'utf8'
                        );

                        let actualRequest = fs.readFileSync(
                            input.requestSchemaFilePath,
                            'utf8'
                        );

                        let actualResponse = fs.readFileSync(
                            input.responseSchemaFilePath,
                            'utf8'
                        );

                        let expectSchema = fs.readFileSync(
                            input.expectSchemaPath,
                            'utf8'
                        );

                        assert.equal(expectTranslation, actualTranslation);

                        assert.isTrue(
                            fs.existsSync(input.configurationFolderPath)
                        );

                        assert.isTrue(
                            fs.existsSync(input.adapterConfigFilePath)
                        );

                        assert.isTrue(fs.existsSync(input.operationFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.translationFolderPath)
                        );

                        assert.equal(
                            expectMockAdapter,
                            actualAdapterConfigFile
                        );

                        assert.isTrue(
                            fs.existsSync(input.operationConfigurationFilePath)
                        );

                        assert.isTrue(fs.existsSync(input.schemaFolderPath));

                        assert.isTrue(
                            fs.existsSync(input.requestSchemaFilePath)
                        );
                        assert.isTrue(
                            fs.existsSync(input.responseSchemaFilePath)
                        );

                        assert.deepEqual(expectSchema, actualRequest);
                        assert.deepEqual(expectSchema, actualResponse);

                        assert.deepEqual(
                            expectOperationConfiguration,
                            actualOperationConfiguration
                        );

                        expect(ctx.stdout).to.contain(
                            `Operation: ${operationName} was created successfully and added to ${serviceName}`
                        );
                    }
                );
        }
    );
});

const deleteFolderRecursive = function(inputPath: string) {
    let files: string[] = [];
    if (fs.existsSync(inputPath)) {
        files = fs.readdirSync(inputPath);
        files.forEach(function(file, index) {
            let curPath = inputPath + '/' + file;
            if (fs.lstatSync(curPath).isDirectory()) {
                deleteFolderRecursive(curPath);
            } else {
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(inputPath);
    }
};

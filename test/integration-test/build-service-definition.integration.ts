import { assert } from 'chai';
import * as fs from 'fs';
import * as path from 'path';
import { cliContainer } from '../../src/inversify.config';
import { BuildServiceDefinitionRepository } from '../../src/orbital-cli.infrastructure/repositories/build-service-definition.repository';
import { ServiceDefinitionFactory } from '../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/factories/service-definition.factory';
import ARTIFACTS from '../../src/constants/artifacts';
import { HTTPVerb } from '../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/http-verb.enum';
import { AdapterType } from '../../src/orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';
describe(`Running build service command`, () => {
    const serviceName = 'ServeGoodMan';
    const operationName = 'goodOperation';

    afterEach(() => {
        process.chdir('../');
        deleteFolderRecursive(serviceName);
    });

    describe('with rest adapter', () => {
        it('should create a bin folder and output a serviceDefinition.json', () => {
            const sync = false;
            const adapterType = AdapterType.REST;
            const validateRequest = true;
            const validateResponse = true;
            const httpEnable = false;
            const verb = HTTPVerb.NONE;
            const verbOnly = false;

            const serviceDefinitionFactory = cliContainer.resolve<
                ServiceDefinitionFactory
            >(ServiceDefinitionFactory);

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                adapterType,
                sync,
                validateRequest,
                validateResponse,
                httpEnable,
                verb,
                verbOnly
            );
            process.chdir(serviceName);

            const buildServiceDefinitionRepository = cliContainer.resolve<
                BuildServiceDefinitionRepository
            >(BuildServiceDefinitionRepository);

            buildServiceDefinitionRepository.BuildServiceDefinition();

            const serviceDefinitionBuildOutputPath = path.join(
                ARTIFACTS.BinFolder,
                ARTIFACTS.ServiceDefinitionFile
            );

            const expectServiceDefinitionPath =
                '../test/integration-test/expect-result/serviceDefinition-rest.json';

            const expect = fs.readFileSync(expectServiceDefinitionPath, 'utf8');

            const actual = fs.readFileSync(
                serviceDefinitionBuildOutputPath,
                'utf8'
            );
            assert.equal(expect, actual);
        });
    });

    describe('with database adapter', () => {
        it('should create a bin folder and output a serviceDefinition.json', () => {
            const sync = false;
            const adapterType = AdapterType.DATABASE;
            const validateRequest = true;
            const validateResponse = true;
            const httpEnable = false;
            const verb = HTTPVerb.NONE;
            const verbOnly = false;

            const serviceDefinitionFactory = cliContainer.resolve<
                ServiceDefinitionFactory
            >(ServiceDefinitionFactory);

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                adapterType,
                sync,
                validateRequest,
                validateResponse,
                httpEnable,
                verb,
                verbOnly
            );
            process.chdir(serviceName);

            const buildServiceDefinitionRepository = cliContainer.resolve<
                BuildServiceDefinitionRepository
            >(BuildServiceDefinitionRepository);

            buildServiceDefinitionRepository.BuildServiceDefinition();

            const serviceDefinitionBuildOutputPath = path.join(
                ARTIFACTS.BinFolder,
                ARTIFACTS.ServiceDefinitionFile
            );

            const expectServiceDefinitionPath =
                '../test/integration-test/expect-result/serviceDefinition-database.json';

            const expect = fs.readFileSync(expectServiceDefinitionPath, 'utf8');

            const actual = fs.readFileSync(
                serviceDefinitionBuildOutputPath,
                'utf8'
            );
            assert.equal(expect, actual);
        });
    });

    describe('with mock adapter', () => {
        it('should create a bin folder and output a serviceDefinition.json', () => {
            const sync = false;
            const adapterType = AdapterType.MOCK;
            const validateRequest = true;
            const validateResponse = true;
            const httpEnable = false;
            const verb = HTTPVerb.NONE;
            const verbOnly = false;

            const serviceDefinitionFactory = cliContainer.resolve<
                ServiceDefinitionFactory
            >(ServiceDefinitionFactory);

            serviceDefinitionFactory.createServiceDefinitionWithOperation(
                serviceName,
                operationName,
                adapterType,
                sync,
                validateRequest,
                validateResponse,
                httpEnable,
                verb,
                verbOnly
            );
            process.chdir(serviceName);

            const buildServiceDefinitionRepository = cliContainer.resolve<
                BuildServiceDefinitionRepository
            >(BuildServiceDefinitionRepository);

            buildServiceDefinitionRepository.BuildServiceDefinition();

            const serviceDefinitionBuildOutputPath = path.join(
                ARTIFACTS.BinFolder,
                ARTIFACTS.ServiceDefinitionFile
            );

            const expectServiceDefinitionPath =
                '../test/integration-test/expect-result/serviceDefinition-mock.json';

            const expect = fs.readFileSync(expectServiceDefinitionPath, 'utf8');

            const actual = fs.readFileSync(
                serviceDefinitionBuildOutputPath,
                'utf8'
            );
            assert.equal(expect, actual);
        });
    });

    const deleteFolderRecursive = function(inputPath: string) {
        let files: string[] = [];
        if (fs.existsSync(inputPath)) {
            files = fs.readdirSync(inputPath);
            files.forEach(function(file, index) {
                let curPath = inputPath + '/' + file;
                if (fs.lstatSync(curPath).isDirectory()) {
                    deleteFolderRecursive(curPath);
                } else {
                    // delete file
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(inputPath);
        }
    };
});

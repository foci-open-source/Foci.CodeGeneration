import { Command, flags } from '@oclif/command';
import { cliContainer } from '../../inversify.config';
import { IPublishServiceDefinitionRepository } from '../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/publish-service-definition.repository.interface';
import TYPES from '../../constants/types';
import inquirer = require('inquirer');

export class Publish extends Command {
    public static description =
        'Publishes service definition file to Consul and an update to RabbitMQ. For more information visit http://orbitalbus.com';
    public static flags = {
        help: flags.help({
            char: 'h',
            description:
                'Publishes service definition file to Consul and an update to RabbitMQ.'
        })
    };

    public static examples = [
        '$ orbital publish',
        '$ orbital publish -h',
        '$ orbital publish --help'
    ];

    public async run() {
        let ps = cliContainer.get<IPublishServiceDefinitionRepository>(
            TYPES.PublishServiceDefinitionRepository
        );

        if ((await ps.checkServiceDefinition())) {
            let message: string = `The service definition already exists in Consul.
If you proceed, the dependents (such as Orbital connectors) on this service would not be able to successfully send messages across Orbital.
Do you want to proceed with the publishing of the service?`;

            let response: any = await inquirer.prompt([
                {
                    default: false,
                    message: message,
                    name: 'publish',
                    type: 'confirm'
                }
            ]);
            if (response.publish) {
                ps.publishServiceDefinition();
            }
        } else {
            ps.publishServiceDefinition();
        }
    }
}

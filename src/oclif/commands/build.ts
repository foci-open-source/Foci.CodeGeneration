import { Command, flags } from '@oclif/command';
import { IBuildServiceDefinitionRepository } from '../../orbital-cli.domain/aggregates/build-service-definition.aggregate/build-service-definition.repository.interface';
import { cliContainer } from '../../inversify.config';
import TYPES from '../../constants/types';

export class Build extends Command {
    public static description = 'Builds the service with its operations and outputs a service definition file. For more information visit http://orbitalbus.com';
    public static flags = {
        help: flags.help({
            char: 'h',
            description: 'Builds the service with its operations and outputs a service definition file.' })
    };

    public static examples = [
        '$ orbital build',
        '$ orbital build -h',
        '$ orbital build --help'
    ];

    public async run() {

        let bs = cliContainer.get<IBuildServiceDefinitionRepository>(
            TYPES.BuildServiceDefinitionRepository
        );
        bs.BuildServiceDefinition();
    }
}

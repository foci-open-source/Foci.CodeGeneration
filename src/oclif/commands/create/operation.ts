import { Command, flags } from '@oclif/command';
import { cliContainer } from '../../../inversify.config';
import { ICommandHandler } from '../../handlers/interfaces/command.handler.interface';
import { CreateOperationDTO } from '../../data-transfers/create-operation.dto';
import TYPES from '../../../constants/types';
import * as inquirer from 'inquirer';
import { AdapterType } from '../../../orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';
import { HTTPVerb } from '../../../orbital-cli.domain/aggregates/create-service-definition.aggregate/http-verb.enum';

export class Operation extends Command {
    public static description =
        'Creates an operation and adds it to the service folder. For more information visit http://orbitalbus.com';
    public static flags = {
        adapter: flags.string({
            char: 'a',
            description:
                'flag to indicate the adapter to use with the operation',
            options: [AdapterType.MOCK, AdapterType.REST, AdapterType.DATABASE]
        }),
        help: flags.help({ char: 'h' }),
        httpEnabled: flags.string({
            description:
                'flag to indicate if operation can be reached by HTTP request',
            options: [
                HTTPVerb.DELETE,
                HTTPVerb.GET,
                HTTPVerb.NONE,
                HTTPVerb.PATCH,
                HTTPVerb.POST,
                HTTPVerb.PUT
            ]
        }),
        sync: flags.string({
            char: 's',
            description: 'flag to indicate if operation is synchronous or asynchronous',
            options: ['true', 'false']
        }),
        validateRequest: flags.string({
            description: 'flag to indicate if validation for the operation request message is needed',
            options: ['true', 'false']
        }),
        validateResponse: flags.string({
            description: 'flag to indicate if validation for the operation response message is needed',
            options: ['true', 'false']
        }),
        verbOnly: flags.string({
            dependsOn: ['httpEnabled'],
            description:
                'flag to indicate if the operation can be reached by setting only the HTTP verb',
            options: ['true', 'false']
        })
    };

    public static args = [
        {
            description: 'operation name to create',
            name: 'operationName'
        }
    ];

    public static examples = [
        '$ orbital create:operation OperationExample -a Adapters.Mock -s false --validateRequest true --validateResponse false --httpEnabled GET --verbOnly true',
        '$ orbital create:operation OperationExample -a Adapters.Rest -s false --validateRequest true --validateResponse false --httpEnabled NONE --verbOnly false',
        '$ orbital create:operation OperationExample -a Adapters.Database -s false --validateRequest true --validateResponse false --httpEnabled NONE --verbOnly false',
        '$ orbital create:operation -h',
        '$ orbital create:operation --help'
    ];

    public async run() {
        const { args, flags } = this.parse(Operation);

        let createOperationHandler = cliContainer.get<
            ICommandHandler<CreateOperationDTO>
        >(TYPES.CreateOperationHandler);

        const dto: CreateOperationDTO = new CreateOperationDTO(args, flags);

        createOperationHandler.execute(dto);
    }
}

import { Command, flags } from '@oclif/command';
import { cliContainer } from '../../../inversify.config';
import { ICommandHandler } from '../../handlers/interfaces/command.handler.interface';
import { CreateServiceDTO } from '../../data-transfers/create-service.dto';
import TYPES from '../../../constants/types';
import * as inquirer from 'inquirer';
import { AdapterType } from '../../../orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';
import { HTTPVerb } from '../../../orbital-cli.domain/aggregates/create-service-definition.aggregate/http-verb.enum';

export class Service extends Command {
    public static description =
        'Creates a service folder with an .orbital file. For more information visit http://orbitalbus.com';
    public static flags = {
        adapter: flags.string({
            char: 'a',
            dependsOn: ['operation'],
            description:
                'flag to indicate the adapter to use with the operation',
            options: [AdapterType.MOCK, AdapterType.REST, AdapterType.DATABASE]
        }),
        help: flags.help({
            char: 'h',
            description: 'Creates a service folder with an .orbital file.'
        }),
        httpEnabled: flags.string({
            dependsOn: ['operation'],
            description:
                'flag to indicate if operation can be reached by HTTP request',
            options: [
                HTTPVerb.DELETE,
                HTTPVerb.GET,
                HTTPVerb.NONE,
                HTTPVerb.PATCH,
                HTTPVerb.POST,
                HTTPVerb.PUT
            ]
        }),
        operation: flags.string({
            char: 'o',
            description: 'optional operation name to create'
        }),
        sync: flags.string({
            char: 's',
            dependsOn: ['operation'],
            description: 'flag to indicate if operation is synchronous or asynchronous',
            options: ['true', 'false']
        }),
        validateRequest: flags.string({
            dependsOn: ['operation'],
            description: 'flag to indicate if validation for the operation request message is needed',
            options: ['true', 'false']
        }),
        validateResponse: flags.string({
            dependsOn: ['operation'],
            description: 'flag to indicate if validation for the operation response message is needed',
            options: ['true', 'false']
        }),
        verbOnly: flags.string({
            dependsOn: ['httpEnabled'],
            description:
                'flag to indicate if the operation can be reached by setting only the HTTP verb',
            options: ['true', 'false']
        })
    };

    public static args = [
        {
            description: 'name of the service to create',
            name: 'serviceName',
            required: true
        }
    ];

    public static examples = [
        '$ orbital create:service ServiceExample',
        '$ orbital create:service ServiceExample -o OperationExample -s true -a Adapters.Mock --validateRequest true --validateResponse true --httpEnabled GET --verbOnly true',
        '$ orbital create:service ServiceExample -o OperationExample -s false -a Adapters.Rest --validateRequest true --validateResponse false --httpEnabled NONE --verbOnly false',
        '$ orbital create:service ServiceExample -o OperationExample -s false -a Adapters.Database --validateRequest true' +
            ' --validateResponse false --httpEnabled NONE --verbOnly false',
        '$ orbital create:service -h',
        '$ orbital create:service --help'
    ];

    public async run() {
        const { args, flags } = this.parse(Service);
        let responses;

        let createServiceHandler = cliContainer.get<
            ICommandHandler<CreateServiceDTO>
        >(TYPES.CreateServiceHandler);

        const dto: CreateServiceDTO = new CreateServiceDTO(
            args,
            flags,
            responses
        );
        createServiceHandler.execute(dto);
    }
}

import { inject, injectable } from 'inversify';
import { ICommandHandler } from './interfaces/command.handler.interface';
import { CreateOperationDTO } from '../data-transfers/create-operation.dto';
import TYPES from '../../constants/types';
import { IServiceDefinitionFactory } from '../../orbital-cli.domain/aggregates/create-service-definition.aggregate/factories/interfaces/service-definition.factory.interface';
import { ServiceNotExistException } from '../../orbital-cli.domain/exceptions/service-not-exist.exception';
import { OperationAlreadyExistException } from '../../orbital-cli.domain/exceptions/operation-already-exist.exception';

@injectable()
export class CreateOperationHandler
    implements ICommandHandler<CreateOperationDTO> {
    constructor(
        @inject(TYPES.ServiceDefinitionFactory)
        private serviceDefinitionFactory: IServiceDefinitionFactory
    ) {}

    public execute(dto: CreateOperationDTO): boolean {
        try {
            this.serviceDefinitionFactory.createOperationAndAddToServiceDefinition(
                dto.operation,
                dto.adapter,
                dto.sync,
                dto.validateRequest,
                dto.validateResponse,
                dto.httpEnable,
                dto.verb,
                dto.verbOnly
            );
        } catch (error) {
            if (error instanceof OperationAlreadyExistException || error instanceof ServiceNotExistException) {
                console.error(error.message);
            } else {
                throw error;
            }
            return false;
        }

        return true;
    }
}

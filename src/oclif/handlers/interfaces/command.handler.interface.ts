export interface ICommandHandler<T> {
    execute(data: T): boolean;
}

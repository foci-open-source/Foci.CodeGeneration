import { inject, injectable } from 'inversify';
import { ICommandHandler } from './interfaces/command.handler.interface';
import { CreateServiceDTO } from '../data-transfers/create-service.dto';
import TYPES from '../../constants/types';
import { IServiceDefinitionFactory } from '../../orbital-cli.domain/aggregates/create-service-definition.aggregate/factories/interfaces/service-definition.factory.interface';
import { OperationAlreadyExistException } from '../../orbital-cli.domain/exceptions/operation-already-exist.exception';

@injectable()
export class CreateServiceHandler implements ICommandHandler<CreateServiceDTO> {
    constructor(
        @inject(TYPES.ServiceDefinitionFactory)
        private serviceDefinitionFactory: IServiceDefinitionFactory
    ) {}

    public execute(dto: CreateServiceDTO): boolean {
        try {
            if (dto.operation) {
                this.serviceDefinitionFactory.createServiceDefinitionWithOperation(
                    dto.serviceName,
                    dto.operation,
                    dto.adapterType,
                    dto.sync,
                    dto.validateRequest,
                    dto.validateResponse,
                    dto.httpEnable,
                    dto.verb,
                    dto.verbOnly
                );
            } else {
                this.serviceDefinitionFactory.createServiceDefinition(
                    dto.serviceName
                );
            }
        } catch (error) {
            if (error instanceof OperationAlreadyExistException) {
                console.error(error.message);
            } else {
                throw error;
            }
            return false;
        }
        return true;
    }
}

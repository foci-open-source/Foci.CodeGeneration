import { AdapterType } from '../../orbital-cli.domain/aggregates/create-service-definition.aggregate/adapter-type.enum';
import { HTTPVerb } from '../../orbital-cli.domain/aggregates/create-service-definition.aggregate/http-verb.enum';

export class CreateServiceDTO {

    private _serviceName: string;
    private _operation?: string;
    private _sync?: boolean;
    private _adapterType?: AdapterType;
    private _validateRequest?: boolean;
    private _validateResponse?: boolean;
    private _httpEnable?: boolean;
    private _verb?: HTTPVerb;
    private _verbOnly?: boolean;

    constructor(args, flags, responses?) {
        this._serviceName = args.serviceName;
        if (flags.operation) {
            this._operation = flags.operation;
            this._sync = flags.sync === 'true';
            this._adapterType = flags.adapter || AdapterType.REST;
            this._validateRequest = flags.validateRequest === 'true';
            this._validateResponse = flags.validateResponse === 'true';

            if (flags.httpEnabled && flags.httpEnabled !== HTTPVerb.NONE) {
                this._httpEnable = true;
                this._verb = flags.httpEnabled;
                this._verbOnly = flags.verbOnly === 'true';
            } else {
                this._httpEnable = false;
                this._verb = HTTPVerb.NONE;
                this._verbOnly = false;
            }
        }
    }

    public get serviceName(): string {
        return this._serviceName;
    }

    public get operation(): string {
        return this._operation;
    }

    public get sync(): boolean {
        return this._sync;
    }

    public get adapterType(): AdapterType {
        return this._adapterType;
    }

    public get validateRequest(): boolean {
        return this._validateRequest;
    }

    public get validateResponse(): boolean {
        return this._validateResponse;
    }

    public get httpEnable(): boolean {
        return this._httpEnable;
    }

    public get verb(): HTTPVerb {
        return this._verb;
    }

    public get verbOnly(): boolean {
        return this._verbOnly;
    }
}

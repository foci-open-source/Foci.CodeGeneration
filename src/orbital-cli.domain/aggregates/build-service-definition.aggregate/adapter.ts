/**
 * The value object class for representing the values for adapter
 */
export class Adapter {
    /**
     * The type of adapter
     */
    private _type: string;

    /**
     * The string representation of the adapter configuration
     */
    private _configuration: string;

    /**
     * The static method for default Adapter
     * @static
     * @returns {Adapter}
     */
    public static default(): Adapter {
        return new Adapter('', '');
    }

    /**
     * Constructor requires adapter type and adapter config to create an instance of Adapter
     * @param {string} type The type of adapter
     * @param {string} configuration The string representation of the configuration
     */
    constructor(type: string, configuration: string) {
        this._type = type;
        this._configuration = configuration;
    }

    /**
     * The getter method of the adapter type property.
     * @type {string}
     */
    public get type(): string {
        return this._type;
    }

    /**
     * The getter method of the adapter configuration property.
     * @type {string}
     */
    public get configuration(): string {
        return this._configuration;
    }

    /**
     * The toJSON method to customize what to output for JSON.stringify()
     */
    public toJSON(): any {
        return {
            configuration: this._configuration,
            type: this._type
        };
    }
}

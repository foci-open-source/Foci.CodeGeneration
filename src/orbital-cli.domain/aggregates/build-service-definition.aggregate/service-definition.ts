import { Operation } from './operation';

/**
 * The aggregate root class of service definition
 */
export class ServiceDefinition {
    /**
     * The name of the service
     * @type {string}
     */
    private _serviceName: string;

    /**
     * The dictionary of operation name and the instances of the operations
     * @type {Map<string, Operation>}
     */
    private _operations: Array<Operation> = new Array<Operation>();

    constructor(serviceName: string) {
        this._serviceName = serviceName;
    }

    /**
     * The getter method of the private property _serviceName
     * @type {string}
     */
    public get serviceName(): string {
        return this._serviceName;
    }

    /**
     * The method to add operation to the service definition with an operation name
     * @param {string} operationName The operation name
     * @returns {Operation} The operation been added to service definition
     */
    public addOperation(operationName: string): Operation {
        let operation: Operation = new Operation(operationName);
        this._operations.push(operation);
        return operation;
    }

    /**
     * The toJSON method to customize what to output for JSON.stringify()
     */
    public toJSON(): any {
        return {
            operations: this._operations,
            serviceName: this._serviceName
        };
    }
}

/**
 * The class encloses a series of properties to set flag for validation when processing the message.
 */
export class Validate {
    /**
     * The flag to set if we want the request the agent receives, either from HTTP calls or
     * Rabbitmq, to be validated when processing the message.
     */
    private _request: boolean = false;

    /**
     * The flag to set if we want the response the agent receives back from the adapter,
     * either from HTTP calls or Rabbitmq, to be validated when processing the message.
     */
    private _response: boolean = false;

    public static default(): Validate {
        return new Validate(false, false);
    }

    /**
     * The constructor that requires two flags to create an instance of validate
     * @param {boolean} request The flag to indicate if the operation should validate the request message
     * @param {boolean} response The flag to indicate if the operation should validate the response message
     */
    constructor(request: boolean, response: boolean) {
        this._request = request;
        this._response = response;
    }
    /**
     * The getter method of the private property _request
     * @type {boolean}
     */
    public get request(): boolean {
        return this._request;
    }

    /**
     * The getter method of the private property _response
     * @type {boolean}
     */
    public get response(): boolean {
        return this._response;
    }

    /**
     * The toJSON method to customize what to output for JSON.stringify()
     */
    public toJSON(): any {
        return {
            request: this._request,
            response: this._response
        };
    }
}

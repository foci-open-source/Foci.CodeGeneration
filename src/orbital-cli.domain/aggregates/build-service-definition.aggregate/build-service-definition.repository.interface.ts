import { ServiceDefinition } from './service-definition';

export interface IBuildServiceDefinitionRepository {
    /**
     * The function to build service definition.
     * @param {ServiceDefinition} serviceDefinition The service definition will be build
     * @returns {ServiceDefinition} The service definition built
     */
    BuildServiceDefinition(): ServiceDefinition;
}

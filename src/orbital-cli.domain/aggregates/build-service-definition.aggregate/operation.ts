import { Adapter } from './adapter';
import { Schemas } from './schemas';
import { HTTPVerb } from '../create-service-definition.aggregate/http-verb.enum';
import { Validate } from './validate-configuration';
import { Http } from './http-configuration';

/**
 * The child entity class for operation
 */
export class Operation {
    /**
     * The name of the operation.
     */
    private _operationName: string;

    /**
     * The boolean representation of the operation is sync or async
     */
    private _sync: boolean = true;

    /**
     * The value object of adapter
     */
    private _adapter: Adapter = Adapter.default();

    /**
     * The value object of schemas
     */
    private _schemas: Schemas = Schemas.default();

    /**
     * The string representation of the translation
     */
    private _translation: string = '';

    /**
     * The configuration to define the http endpoints to contact the Agent
     */
    private _httpConfiguration: Http = Http.default();

    /**
     * Flags to indicate if the Request or Response message should be validated.
     */
    private _validate: Validate = Validate.default();

    /**
     * The constructor that requires an operation name to create an instance of operation
     * @param {string} operationName The name of the operation
     */
    constructor(operationName: string) {
        this._operationName = operationName;
    }

    /**
     * The getter method of the private property _operationName
     * @type {string}
     */
    public get operationName(): string {
        return this._operationName;
    }

    /**
     * The getter method of the private property _sync
     * @type {boolean}
     */
    public get sync(): boolean {
        return this._sync;
    }

    /**
     * The getter method of the private property _adapter
     * @type {Adapter}
     */
    public get adapter(): Adapter {
        return this._adapter;
    }

    /**
     * The getter method of the private property _schemas
     * @type {Schemas}
     */
    public get schemas(): Schemas {
        return this._schemas;
    }

    /**
     * The getter method of the private property _translation
     * @type {string}
     */
    public get translation(): string {
        return this._translation;
    }

    /**
     * The getter method of the private property _validate
     * @type {boolean}
     */
    public get validate(): Validate {
        return this._validate;
    }

    /**
     * The getter method of the private property _httpConfiguration
     * @type {Http}
     */
    public get httpConfiguration(): Http {
        return this._httpConfiguration;
    }

    /**
     * The method to add adapter to the operation
     * @param {string} type The string representation of the adapter type
     * @param {string} configuration The string representation of the adapter configuration
     * @returns {Operation} The operation with adapter added
     */
    public addAdapter(type: string, configuration: string): Operation {
        this._adapter = new Adapter(type, configuration);
        return this;
    }

    /**
     * The method to add schemas to the operation
     * @param {string} request The string representation of the request schema
     * @param {string} response The string representation of the response schema
     * @returns {Operation} The operation with schemas added
     */
    public addSchemas(request: string, response: string): Operation {
        this._schemas = new Schemas(request, response);
        return this;
    }

    /**
     * The method to add translation to the operation
     * @param {string} translation The string representation of the translation
     * @returns {Operation} The operation with translation added
     */
    public addTranslation(translation: string): Operation {
        this._translation = translation;
        return this;
    }

    /**
     * The method to add operation configuration to the operation
     * @param {string} translation The string representation of the operation configuration
     * @returns {Operation} The operation with operation configuration added
     */
    public addOperationSync(isSync: boolean): Operation {
        this._sync = isSync;
        return this;
    }

    /**
     * The method to add validation of request and response message to the operation
     * @param request flag to indicate if the request message should be validated
     * @param response flag to indicate if the response message should be validated
     */
    public addValidation(request: boolean, response: boolean): Operation {
        this._validate = new Validate(request, response);
        return this;
    }

    /**
     * The method to add http configuration to the operation
     * @param enabled flag to indicate if http is enabled for the operation
     * @param httpVerb Verb to be used in a http request
     * @param verbOnly flag to indicate if the operation can be reached by indicating the verb in the http request
     */
    public addHttpConfiguration(
        enabled: boolean,
        httpVerb: HTTPVerb,
        verbOnly: boolean
    ): Operation {
        this._httpConfiguration = new Http(enabled, httpVerb, verbOnly);
        return this;
    }

    /**
     * The toJSON method to customize what to output for JSON.stringify()
     */
    public toJSON(): any {
        return {
            adapter: this._adapter,
            http: this._httpConfiguration,
            name: this._operationName,
            schemas: this._schemas,
            sync: this._sync,
            translation: this._translation,
            validate: this._validate
        };
    }
}

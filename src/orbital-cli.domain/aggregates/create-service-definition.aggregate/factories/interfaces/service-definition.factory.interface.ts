import { ServiceDefinition } from '../../service-definition';
import { AdapterType } from '../../adapter-type.enum';
import { HTTPVerb } from '../../http-verb.enum';

export interface IServiceDefinitionFactory {
    /**
     * This function is used to create the default service definition with orbital file.
     * @param {string} name the name of the service definition
     * @returns {ServiceDefinition} the service definition with orbital file
     */
    createServiceDefinition(name: string): ServiceDefinition;

    /**
     * This function is used to create the default service definition with orbital file,
     * default adapter, default schemas, default translation and default operation
     * configuration
     * @param {string} serviceName the name of the service definition
     * @param {string} operationName the name of the operation
     * @param {AdapterType} adapterType the type of the adapter
     * @param {boolean} sync the boolean value to indicate the operation is sync or not
     * @param {boolean} validateRequest the flag to indicate if the operation should validate the request message
     * @param {boolean} validateResponse the flag to indicate if the operation should validate the response message
     * @param {boolean} httpEnable the flag to indicate if the operation will be accessible through HTTP Endpoints.
     * @param {HTTPVerb} verb  HTTP method (GET,PUT,POST) we want to relate to the operation
     * @param {boolean} verbOnly the flag is to indicate that operation can be hit by an HTTP without specifying the operation.
     * @returns {ServiceDefinition} the service definition with orbital file default adapter,
     * default schemas, default translation and default operation configuration
     */
    createServiceDefinitionWithOperation(
        serviceName: string,
        operationName: string,
        adapterType: AdapterType,
        sync: boolean,
        validateRequest: boolean,
        validateResponse: boolean,
        httpEnable: boolean,
        verb: HTTPVerb,
        verbOnly: boolean
    ): ServiceDefinition;

    /**
     * This function is used to create the operation with exist service name
     * with adapter based on the input adapter type, default schemas, default
     * translation and operation configuration based on the input sync value
     * configuration
     * @param {string} operationName the name of the operation
     * @param {AdapterType} adapterType the type of the adapter
     * @param {boolean} sync the boolean value to indicate the operation is sync or not
     * @param {boolean} validateRequest the flag to indicate if the operation should validate the request message
     * @param {boolean} validateResponse the flag to indicate if the operation should validate the response message
     * @param {boolean} httpEnable the flag to indicate if the operation will be accessible through HTTP Endpoints.
     * @param {HTTPVerb} verb  HTTP method (GET,PUT,POST) we want to relate to the operation
     * @param {boolean} verbOnly the flag is to indicate that operation can be hit by an HTTP without specifying the operation.
     * @returns {ServiceDefinition} the service definition with adapter based on the input adapter type,
     * default schemas, default translation and operation configuration based on the input sync value
     */
    createOperationAndAddToServiceDefinition(
        operationName: string,
        adapterType: AdapterType,
        sync: boolean,
        validateRequest: boolean,
        validateResponse: boolean,
        httpEnable: boolean,
        verb: HTTPVerb,
        verbOnly: boolean
    ): ServiceDefinition;
}

import { inject, injectable } from 'inversify';
import { ITemplateService } from '../services/interfaces/template.service.interface';
import { IServiceDefinitionFactory } from './interfaces/service-definition.factory.interface';
import { ICreateServiceDefinitionRepository } from '../create-service-definition.repository.interface';
import { ServiceDefinition } from '../service-definition';
import TYPES from '../../../../constants/types';
import { Validate } from '../validate-configuration';
import { Http } from '../http-configuration';
import { AdapterType } from '../adapter-type.enum';
import { HTTPVerb } from '../http-verb.enum';

/**
 * This class is responsible for the creation of service definition and its
 * components.
 */
@injectable()
export class ServiceDefinitionFactory implements IServiceDefinitionFactory {
    constructor(
        @inject(TYPES.TemplateService)
        private templateService: ITemplateService,
        @inject(TYPES.CreateServiceDefinitionRepository)
        private serviceDefinitionRepository: ICreateServiceDefinitionRepository
    ) {}

    /**
     * @inheritdoc
     */
    public createServiceDefinition(name: string): ServiceDefinition {
        let serviceDefinition: ServiceDefinition = new ServiceDefinition(name);

        serviceDefinition.addOrbitalFile(
            this.templateService.renderOrbitalTemplate(name)
        );

        this.serviceDefinitionRepository.createServiceDefinition(
            serviceDefinition
        );

        return serviceDefinition;
    }

    /**
     * @inheritdoc
     */
    public createServiceDefinitionWithOperation(
        serviceName: string,
        operationName: string,
        adapterType: AdapterType,
        sync: boolean,
        validateRequest: boolean,
        validateResponse: boolean,
        httpEnable: boolean,
        verb: HTTPVerb,
        verbOnly: boolean
    ): ServiceDefinition {
        let serviceDefinition: ServiceDefinition = new ServiceDefinition(
            serviceName
        );

        serviceDefinition.addOrbitalFile(
            this.templateService.renderOrbitalTemplate(serviceName)
        );

        serviceDefinition = this.renderOperationToServiceDefinition(
            serviceDefinition,
            operationName,
            sync,
            adapterType,
            new Validate(validateRequest, validateResponse),
            new Http(httpEnable, verb, verbOnly)
        );

        this.serviceDefinitionRepository.createServiceDefinition(
            serviceDefinition
        );

        return serviceDefinition;
    }

    /**
     * @inheritdoc
     */
    public createOperationAndAddToServiceDefinition(
        operationName: string,
        adapterType: AdapterType,
        sync: boolean,
        validateRequest: boolean,
        validateResponse: boolean,
        httpEnable: boolean,
        verb: HTTPVerb,
        verbOnly: boolean
    ): ServiceDefinition {
        let serviceDefinition: ServiceDefinition = this.serviceDefinitionRepository.getServiceDefinition();

        serviceDefinition = this.renderOperationToServiceDefinition(
            serviceDefinition,
            operationName,
            sync,
            adapterType,
            new Validate(validateRequest, validateResponse),
            new Http(httpEnable, verb, verbOnly)
        );

        this.serviceDefinitionRepository.addOperationToExistServiceDefinition(
            serviceDefinition,
            operationName
        );

        return serviceDefinition;
    }

    /**
     * The function to render the operation to the service definition.
     * @param {ServiceDefinition} serviceDefinition The service definition to be rendered
     * @param {string} operationName The operation name to be rendered to the service definition
     * @param {boolean} sync The optional flag to set the operation is sync or async
     * @param {AdapterType} adapterType The adapter type
     * @param {Validate} validate  The class encloses a series of properties to set flag for validation when processing the message.
     * @param {Http} http HTTP Configuration that encloses a series of properties to define the http endpoints to contact the Agent.
     * @returns {ServiceDefinition} The rendered service definition with operation
     */
    private renderOperationToServiceDefinition(
        serviceDefinition: ServiceDefinition,
        operationName: string,
        sync: boolean,
        adapterType: AdapterType,
        validate: Validate,
        http: Http
    ): ServiceDefinition {
        let [configuration, adapter]: [
            string,
            string
        ] = this.renderAdapterConfigurationBasedOnAdapterType(adapterType);

        let request: string = this.templateService.renderDefaultSchemaTemplate();
        let response: string = this.templateService.renderDefaultSchemaTemplate();

        let translation: string = this.templateService.renderDefaultTranslationTemplate();

        let operationConfiguration: string = this.templateService.renderOperationConfigurationTemplate(
            http,
            sync,
            validate
        );

        serviceDefinition
            .addOperation(operationName)
            .addAdapter(adapterType, configuration, adapter)
            .addHttpConfiguration(http.enabled, http.verb, http.verbOnly)
            .addValidation(validate.request, validate.response)
            .addSchemas(request, response)
            .addTranslation(translation)
            .addOperationConfiguration(operationConfiguration);

        return serviceDefinition;
    }

    /**
     * Render the adapter configuration and string representation of adapter file based
     * on the adapter type
     * @param {AdapterType} adapterType the nullable adapter type
     * @returns {[string, string]} the adapter configuration and string representation of adapter file
     */
    private renderAdapterConfigurationBasedOnAdapterType(
        adapterType: AdapterType
    ): [string, string] {
        let configuration: string;
        let adapter: string;
        switch (adapterType) {
            case AdapterType.REST:
                configuration = this.templateService.renderDefaultAdapterConfigTemplate();
                adapter = this.templateService.renderRestAdapterConfigTemplate();
                break;
            case AdapterType.MOCK:
                configuration = this.templateService.renderMockDefaultAdapterConfigTemplate();
                adapter = this.templateService.renderMockAdapterConfigTemplate();
                break;
            case AdapterType.DATABASE:
                configuration = this.templateService.renderDatabaseDefaultAdapterConfigTemplate();
                adapter = this.templateService.renderDatabaseAdapterConfigTemplate();
                break;
            default:
                configuration = this.templateService.renderDefaultAdapterConfigTemplate();
                adapter = this.templateService.renderRestAdapterConfigTemplate();
        }
        return [configuration, adapter];
    }
}

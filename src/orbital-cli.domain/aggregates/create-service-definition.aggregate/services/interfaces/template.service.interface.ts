import { Operation } from '../../operation';
import { Http } from '../../http-configuration';
import { Validate } from '../../validate-configuration';

export interface ITemplateService {
    /**
     * This function is used to render orbital file to a orbital file string
     * @returns {string} the string representation of the orbital file with default configuration
     */
    renderOrbitalTemplate(serviceName: string): string;

    /**
     * This function is used to render default database adapter config template to a string
     * representation of the default database adapter configuration
     * @returns {string} the string representation of the default database adapter configuration
     */
    renderDatabaseDefaultAdapterConfigTemplate(): string;

    /**
     * This function is used to render default adapter config template to a string
     * representation of the default adapter configuration
     * @returns {string} the string representation of the default adapter configuration
     */
    renderDefaultAdapterConfigTemplate(): string;

    /**
     * This function is used to render default mock adapter config template to a string
     * representation of the default mock adapter configuration
     * @returns {string} the string representation of the default mock adapter configuration
     */
    renderMockDefaultAdapterConfigTemplate(): string;

    /**
     * This function is used to render default schema template to a string
     * representation of the default schema
     * @returns {string} the string representation of the default schema
     */
    renderDefaultSchemaTemplate(): string;

    /**
     * This function is used to render default translation template to a string
     * representation of the default translation
     * @returns {string} the string representation of the default translation
     */
    renderDefaultTranslationTemplate(): string;

    /**
     * This function is used to render sync operation configuration to a string
     * representation of the sync operation configuration
     * @param {Operation} Operation The operation to be rendered to the template
     * @returns {string} the string representation of the sync operation configuration
     */
    renderOperationConfigurationTemplate(
        httpConfiguration: Http,
        sync: boolean,
        validateConfiguration: Validate
    ): string;

    /**
     * This function is used to render rest adapter configuration to a string
     * representation
     * @param {Adapter} adapter The adapter value object will be rendered
     * @returns {string} the string representation of the rest adapter configuration
     */
    renderRestAdapterConfigTemplate(): string;

    /**
     * This function is used to render mock adapter configuration to a string
     * representation
     * @param {Adapter} adapter The adapter value object will be rendered
     * @returns {string} the string representation of the mock adapter configuration
     */
    renderMockAdapterConfigTemplate(): string;

    /**
     * This function is used to render database adapter configuration to a string
     * representation
     * @param {Adapter} adapter The adapter value object will be rendered
     * @returns {string} the string representation of the database adapter configuration
     */
    renderDatabaseAdapterConfigTemplate(): string;
}

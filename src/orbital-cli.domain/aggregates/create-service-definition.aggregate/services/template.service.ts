import { injectable } from 'inversify';
import * as Mustache from 'mustache';
import { ITemplateService } from './interfaces/template.service.interface';
import TEMPLATES from '../templates';
import { AdapterType } from '../adapter-type.enum';
import { Http } from '../http-configuration';
import { Validate } from '../validate-configuration';

/**
 * This class is responsible for managing the template
 * rending in the Orbital CLI application.
 */
@injectable()
export class TemplateService implements ITemplateService {
    /**
     *  @inheritdoc
     */
    public renderOrbitalTemplate(serviceName: string): string {
        let service = {
            serviceName: serviceName
        };
        let template: string = this.render(service, TEMPLATES.orbitalTemplate);
        return template;
    }

    /**
     *  @inheritdoc
     */
    public renderDefaultAdapterConfigTemplate(): string {
        let template: string = TEMPLATES.adapterConfigurationTemplate;

        return template;
    }

    /**
     *  @inheritdoc
     */
    public renderMockDefaultAdapterConfigTemplate(): string {
        let template: string = TEMPLATES.adapterMockConfigurationTemplate;
        return template;
    }

    /**
     *  @inheritdoc
     */
    public renderDatabaseDefaultAdapterConfigTemplate(): string {
        let template: string = TEMPLATES.adapterDatabaseConfigurationTemplate;
        return template;
    }

    /**
     *  @inheritdoc
     */
    public renderDefaultSchemaTemplate(): string {
        let template: string = TEMPLATES.schemaTemplate;

        return template;
    }

    /**
     *  @inheritdoc
     */
    public renderDefaultTranslationTemplate(): string {
        let template: string = TEMPLATES.translationTemplate;

        return template;
    }

    /**
     *  @inheritdoc
     */
    public renderOperationConfigurationTemplate(
        httpConfiguration: Http,
        sync: boolean,
        validateConfiguration: Validate
    ): string {
        let operationToRender = {
            http: this.renderHTTPConfiguration(httpConfiguration),
            sync: sync,
            validate: this.renderValidateConfiguration(validateConfiguration)
        };

        let template: string = this.render(
            operationToRender,
            TEMPLATES.operationConfigurationTemplate
        );

        return template;
    }

    /**
     *  @inheritdoc
     */
    public renderRestAdapterConfigTemplate(): string {
        let configuration: string = this.renderDefaultAdapterConfigTemplate();
        let adapter = {
            configuration: configuration,
            type: AdapterType.REST
        };
        let template: string = this.render(
            adapter,
            TEMPLATES.adapterConfigTemplate
        );
        return template;
    }

    /**
     *  @inheritdoc
     */
    public renderMockAdapterConfigTemplate(): string {
        let configuration: string = this.renderMockDefaultAdapterConfigTemplate();
        let adapter = {
            configuration: configuration,
            type: AdapterType.MOCK
        };

        let template: string = this.render(
            adapter,
            TEMPLATES.adapterConfigTemplate
        );
        return template;
    }

    /**
     *  @inheritdoc
     */
    public renderDatabaseAdapterConfigTemplate(): string {
        let configuration: string = this.renderDatabaseDefaultAdapterConfigTemplate();
        let adapter = {
            configuration: configuration,
            type: AdapterType.DATABASE
        };

        let template: string = this.render(
            adapter,
            TEMPLATES.adapterConfigTemplate
        );
        return template;
    }

    /**
     *  @inheritdoc
     */
    public renderHTTPConfiguration(httpConfiguration: Http): string {
        let configuration = {
            enabled: httpConfiguration.enabled,
            verb: httpConfiguration.verb,
            verbOnly: httpConfiguration.verbOnly
        };

        let template: string = this.render(configuration, TEMPLATES.http);
        return template;
    }

    private renderValidateConfiguration(
        validateConfiguration: Validate
    ): string {
        let configuration = {
            request: validateConfiguration.request,
            response: validateConfiguration.response
        };
        let template: string = this.render(configuration, TEMPLATES.validate);
        return template;
    }

    /**
     * This function is used to render object to a template string
     * @param {object} resource the resource object will be rendered with template
     * @param {string} path the path of the template
     * @returns {string} the string representation of the rendered resource
     */
    private render(resource: object, template: string): string {
        return Mustache.render(template, resource);
    }
}

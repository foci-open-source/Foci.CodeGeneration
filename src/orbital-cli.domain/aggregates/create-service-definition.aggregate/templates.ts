const TEMPLATES = {
    adapterConfigTemplate: `{
    "type": "{{type}}",
    "configuration":
        {{{configuration}}}
}`,
    adapterConfigurationTemplate: `{
            "uri": "",
            "method": "GET",
            "headers": [
                {
                    "key": "Content-Type",
                    "value": "application/json"
                }
            ]
        }`,
    adapterDatabaseConfigurationTemplate:
        `{"statement":"EXECUTE","type":"MYSQL","sql":` +
        `"INSERT INTO Customer ( CustomerId, LastName, FirstName ) VALUES ( @id, @lastName, @firstName );","connection":` +
        `"Server=localhost;Database=Customer;Uid=root;Pwd=example;","parameters":{"id":"1","lastName":"Goodman","firstName":"Saul"}}`,
    adapterMockConfigurationTemplate:
        `{"selector":"$.Id","conditions":[{"operator":"==","compareTo":"48","exception` +
        `":"What is this"},{"operator":"==","compareTo":"112","response":"{\\"id\\":112,` +
        `\\"name\\":{\\"firstName\\":\\"John\\",\\"middleName\\":\\"Jane\\",\\"lastName\\":\\` +
        `"Doe\\"},\\"address\\":{\\"addressLine1\\":\\"5 City Street\\",\\"addressLine2\\":\\"\\` +
        `",\\"city\\":\\"Ottawa\\",\\"provinceState\\":\\"ON\\",\\"country\\":\\"CA\\",\\"postalZipCode` +
        `\\":\\"5A5A5A\\"}}"},{"operator":">","compareTo":"73","response":"{\\"id\\":24,\\"name\\":{\\"firstName` +
        `\\":\\"John\\",\\"middleName\\":\\"Not\\",\\"lastName\\":\\"Smith\\"},\\"address\\":{\\"streetNumber` +
        `\\":123,\\"addressLine1\\":\\"East Lake Forest Street\\",\\"city\\":\\"Fishingdale\\",\\"provinceState` +
        `\\":\\"NY\\",\\"country\\":\\"US\\",\\"postalZipCode\\":\\"54321\\"}}"},{"operator":"<","compareTo":"73"` +
        `,"response":"{\\"id\\":42,\\"name\\":{\\"firstName\\":\\"John\\",\\"middleName\\":\\"Ofcourse\\",\\"` +
        `lastName\\":\\"Smith\\"},\\"address\\":{\\"streetNumber\\":694,\\"addressLine1\\":\\"North Lake Forest ` +
        `Street\\",\\"city\\":\\"Farmingdale\\",\\"provinceState\\":\\"NY\\",\\"country\\":\\"US\\",\\"postalZipCode` +
        `\\":\\"11735\\"}}"}],"default":{"response": "{\\"response\\": \\"Default Response\\"}"}}`,
    http: `{
            "enabled": {{enabled}},
            "verb": "{{verb}}",
            "verbOnly": {{verbOnly}}
        }`,
    operationConfigurationTemplate: `{
    "sync": {{sync}},
    "validate": {{{validate}}},
    "http": {{{http}}}
}`,
    orbitalTemplate: `{
    "serviceName": "{{serviceName}}",
    "consul": {
        "address": "localhost",
        "port": 8500
    },
    "rabbit": {
        "address": "localhost",
        "port": 5672,
        "username": "guest",
        "password": "guest"
    }
}`,
    schemaTemplate: `{
    "title": "",
    "description": "",
    "type": "object",
    "typeName": "",
    "properties": {
        "id": {
            "type": "string"
        }
    }
}`,
    translationTemplate: `function serviceInboundTranslation(e) {
    return {
        Id: e.id,
        Name: { FirstName: 'John', MiddleName: 'Jane', LastName: 'Doe' },
        Address: {
            StreeNumber: 5,
            AddressLine1: 'City Street',
            AddressLine2: '',
            City: 'Ottawa',
            ProvinceState: 'ON',
            Country: 'CA',
            PostalZipCode: '5A5A5A'
        }
    };
}
function getDynamicParameters(e) {
    var r = { customerId: e.id };
    return (
        47 == e.id &&
            throwBusinessError('Bad first and last names', {
                messageType: 'string'
            }),
        r
    );
}
function serviceOutboundTranslation(e) {
    return e;
}`,
    validate: `{
            "request": {{request}},
            "response": {{response}}
        }`
};

export default TEMPLATES;

import { ServiceDefinition } from './service-definition';

export interface ICreateServiceDefinitionRepository {
    /**
     * The function to create service definition.
     * @param {ServiceDefinition} serviceDefinition The service definition will be created
     * @returns {ServiceDefinition} The service definition created
     */
    createServiceDefinition(
        serviceDefinition: ServiceDefinition
    ): ServiceDefinition;

    /**
     * The function to get the service definition from orbital file
     * @returns {ServiceDefinition} The service found in orbital file
     */
    getServiceDefinition(): ServiceDefinition;

    /**
     * The function to add operation to an existing service definition
     * @param {ServiceDefinition} serviceDefinition The service definition to add an operation
     * @param {string} operationName The operation name will be added to the service definition
     * @returns {ServiceDefinition} The service definition finished adding the operation
     */
    addOperationToExistServiceDefinition(
        serviceDefinition: ServiceDefinition,
        operationName: string,
    ): ServiceDefinition;
}

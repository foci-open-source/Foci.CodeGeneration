import { HTTPVerb } from './http-verb.enum';

export class Http {
    /**
     *  The flag to indicate if the operation will be
     *  accessible through HTTP Endpoints.
     */
    private _enabled: boolean = false;

    /**
     * HTTP method (GET,PUT,POST) we want to relate to the operation
     */
    private _verb: HTTPVerb;

    /**
     * The flag is to indicate that operation can be hit by an HTTP
     * without specifying the operation.
     */
    private _verbOnly: boolean = false;

    public static default(): Http {
        return new Http(false, HTTPVerb.NONE, false);
    }

    /**
     * The constructor that requires flags and http verb to create an instance of http
     * @param {boolean} enabled flag to indicate if http configuration is enabled
     * @param {HTTPVerb} verb http verb to be used when reaching a service through HTTP endpoints
     * @param {boolean} verbOnly flag to indicate that the operation may be reached just by indicating the http verb and service name
     */
    constructor(enabled: boolean, verb: HTTPVerb, verbOnly: boolean) {
        this._enabled = enabled;
        this._verb = verb;
        this._verbOnly = verbOnly;
    }

    /**
     * The getter method of the private property _enabled
     * @type {boolean}
     */
    public get enabled(): boolean {
        return this._enabled;
    }

    /**
     * The getter method of the private property _verb
     * @type {HTTPVerb}
     */
    public get verb(): HTTPVerb {
        return this._verb;
    }

    /**
     * The getter method of the private property _verbOnly
     * @type {boolean}
     */
    public get verbOnly(): boolean {
        return this._verbOnly;
    }
}

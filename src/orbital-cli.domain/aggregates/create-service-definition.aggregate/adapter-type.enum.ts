/**
 * The enumeration type of AdapterType
 */
export enum AdapterType {
    REST = 'Adapters.Rest',
    MOCK = 'Adapters.Mock',
    DATABASE = 'Adapters.Database'
}

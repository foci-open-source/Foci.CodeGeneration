/**
 * The enumeration type of HTTPVerb
 */
export enum HTTPVerb {
    NONE = 'NONE',
    GET = 'GET',
    PUT = 'PUT',
    POST = 'POST',
    PATCH = 'PATCH',
    DELETE = 'DELETE'
}

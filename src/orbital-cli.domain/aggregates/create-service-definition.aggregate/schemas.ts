/**
 * The value object class of schemas
 */
export class Schemas {
    private _request: string;
    private _response: string;

    public static default(): Schemas {
        return new Schemas('', '');
    }
    /**
     * The constructor requires request schema and response schema to create an instance of schemas
     * @param {string} request The string representation of request schema
     * @param {string} response The string representation of response schema
     */
    constructor(request: string, response: string) {
        this._request = request;
        this._response = response;
    }

    /**
     * The getter method of private method _request
     * @type {string}
     */
    public get request(): string {
        return this._request;
    }

    /**
     * The getter method of private method _response
     * @type {string}
     */
    public get response(): string {
        return this._response;
    }
}

import { Operation } from './operation';

/**
 * The aggregate root class of service definition
 */
export class ServiceDefinition {
    /**
     * The name of the service
     * @type {string}
     */
    private _serviceName: string;

    /**
     * The string representation of the orbital config file
     * @type {string}
     */
    private _orbital: string = '';

    /**
     * The dictionary of operation name and the instances of the operations
     * @type {Map<string, Operation>}
     */
    private _operations: Map<string, Operation> = new Map<string, Operation>();

    /**
     * The constructor requires a service name to create an instance of ServiceDefinition.
     * @param {string} serviceName The name of the service
     */
    constructor(serviceName: string) {
        if (serviceName === '') {
            throw new Error('The service name can not be empty');
        }
        this._operations = new Map<string, Operation>();
        this._serviceName = serviceName;
    }

    /**
     * The getter method of the private property _serviceName
     * @type {string}
     */
    public get serviceName(): string {
        return this._serviceName;
    }

    /**
     * The getter method of the private property _orbital
     * @type {string}
     */
    public get orbitalFile(): string {
        return this._orbital;
    }

    /**
     * The getter method of the private property _operations
     * @type {Map<string, Operation>}
     */
    public get operations(): Map<string, Operation> {
        return this._operations;
    }

    /**
     * The getter method of operation with a operation name.
     * @param {string} operationName The operation name need to retrieve from operations
     * @returns {Operation} The operation found in operation map
     */
    public getOperation(operationName: string): Operation {
        const operation = this.operations.get(operationName);
        if (operation === undefined) {
            throw new Error(
                `When trying to retrieve operation ${operationName} from ${
                    this._serviceName
                } the operation is undefined`
            );
        }
        return operation;
    }

    /**
     * The method to add orbital file string to the service definition
     * @param {string} orbital The string representation of the orbital file
     */
    public addOrbitalFile(orbital: string) {
        this._orbital = orbital;
    }

    /**
     * The method to add operation to the service definition with an operation name
     * @param {string} operationName The operation name
     * @returns {Operation} The operation been added to service definition
     */
    public addOperation(operationName: string): Operation {
        let operation: Operation = new Operation(operationName);
        this._operations.set(operationName, operation);
        return operation;
    }
}

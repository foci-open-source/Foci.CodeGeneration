export interface IPublishServiceDefinitionRepository {

    /**
     * Publishes the service definition to Consul and notifies the Agent through RabbitMQ.
     */
    publishServiceDefinition(): void;

    /**
     *
     * Check the service definition is exist in consul or not.
     */
    checkServiceDefinition(): Promise<boolean>;

}

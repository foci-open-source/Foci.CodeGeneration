export class ConsulConfiguration {
    /**
     * The string representation of Consul's address
     * @type {string}
     */
    private _address: string;

    /**
     * The string representation of Consul's port
     */
    private _port: string;

    constructor(address: string, port: string) {
        this._address = address;
        this._port = port;
    }

    /**
     * The getter method of private method _port
     * @type {string}
     */
    public get port(): string {
        return this._port;
    }

    /**
     * The getter method of private method _address
     * @type {string}
     */
    public get address(): string {
        return this._address;
    }
}

export class UpdateService {
    private servicename: string;

    public get serviceName(): string {
        return this.servicename;
    }

    /**
     * Constructor requires a service name to be able to publish it to RabbitMQ.
     * @param serviceName The string representation of the service name.
     */
    constructor(serviceName: string) {
        this.servicename = serviceName;
    }
}

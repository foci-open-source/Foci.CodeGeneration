import { ConsulConfiguration } from './consul-configuration';
import { Rabbitmq } from './rabbitmq-configuration';
export class OrbitalConfiguration {
    /**
     * Consul's configuration to be able to make requests.
     */
    private _consul: ConsulConfiguration;

    /**
     * RabbitMQ's configuration to be able to make requests.
     */
    private _rabbitmq: Rabbitmq;

    private _serviceName: string;

    constructor(rabbitmq: Rabbitmq, consul: ConsulConfiguration, serviceName: string) {
        this._consul = consul;
        this._rabbitmq = rabbitmq;
        this._serviceName = serviceName;
    }

    /**
     * The getter method of private method _rabbitmq
     * @type {Rabbitmq}
     */
    public get rabbitmq(): Rabbitmq {
        return this._rabbitmq;
    }

    /**
     * The getter method of private method _consul
     * @type {ConsulConfiguration}
     */
    public get consul(): ConsulConfiguration {
        return this._consul;
    }

    public get serviceName(): string {
        return this._serviceName;
    }
}

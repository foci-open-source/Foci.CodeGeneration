export class Rabbitmq {
    /**
     * string representation of RabbitMQ's address.
     * @type {string}
     */
    private _address: string;

    /**
     * string representation of RabbitMQ's port.
     * @type {string}
     */
    private _port: string;

    /**
     * string representation of RabbitMQ's username credential
     * @type {string}
     */
    private _username: string;

    /**
     * string representation of RabbitMQ's password credential
     * @type {string}
     */
    private _password: string;

    constructor(address: string, port: string, username: string, password: string) {
        this._address = address;
        this._port = port;
        this._username = username;
        this._password = password;
    }
    /**
     * The getter method of private method _address
     * @type {string}
     */
    public get address(): string {
        return this._address;
    }

    /**
     * The getter method of private method _port
     * @type {string}
     */
    public get port(): string {
        return this._port;
    }

    /**
     * The getter method of private method _username
     * @type {string}
     */
    public get username(): string {
        return this._username;
    }

    /**
     * The getter method of private method _password
     * @type {string}
     */
    public get password(): string {
        return this._password;
    }
}

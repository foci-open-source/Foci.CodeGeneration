export class ServiceNotExistException extends Error {
    constructor(message: string) {
        super(message);
    }
}

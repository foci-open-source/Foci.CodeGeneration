export class ServiceAlreadyExistException extends Error {
    constructor(message: string) {
        super(message);
    }
}

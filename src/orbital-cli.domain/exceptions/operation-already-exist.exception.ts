export class OperationAlreadyExistException extends Error {
    constructor(message: string) {
        super(message);
    }
}

export class FileDoesntExistException extends Error {
    constructor(message: string) {
        super(message);
    }
}

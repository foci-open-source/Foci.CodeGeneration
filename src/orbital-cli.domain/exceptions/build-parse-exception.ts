export class BuildParseException extends Error {
    constructor(message: string) {
        super(message);
    }
}

const CONNECTIONS = {
    agentqueue : 'orbital.service.publish',
    typeAgent: 'Foci.Orbital.Agent.Models.UpdateService:Foci.Orbital.Agent'
};

export default CONNECTIONS;

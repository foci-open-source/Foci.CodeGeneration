const ARTIFACTS = {
    AdapterConfigFile: 'adapterConfig.json',
    AdapterConfigurationsFolder: 'adapterConfigurations',
    BinFolder: 'bin',
    OperationConfigurationFile: 'operationConfiguration.json',
    OperationFolder: 'operation',
    OrbitalFile: '.orbital',
    RequestSchemaFile: 'request.json',
    ResponseSchemaFile: 'response.json',
    SchemasFolder: 'schemas',
    ServiceDefinitionFile: 'serviceDefinition.json',
    TranslationFile: 'translation.js',
    TranslationsFolder: 'translations'
};

export default ARTIFACTS;

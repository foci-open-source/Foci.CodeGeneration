const TYPES = {
    AmqpRepository: Symbol.for('IAmqpRepository'),
    BuildServiceDefinitionParser: Symbol.for('IBuildServiceDefinitionParser'),
    BuildServiceDefinitionRepository: Symbol.for('IBuildServiceDefinitionRepository'),
    ConsulRepository: Symbol.for('IConsulRepository'),
    CreateOperationHandler: Symbol.for('ICommandHandler<CreateOperationDTO>'),
    CreateServiceDefinitionRepository: Symbol.for('ICreateServiceDefinitionRepository'),
    CreateServiceHandler: Symbol.for('ICommandHandler<CreateServiceDTO>'),
    FileRepository: Symbol.for('IFileRepository'),
    PublishServiceDefinitionRepository: Symbol.for('IPublishServiceDefinitionRepository'),
    ServiceDefinitionFactory: Symbol.for('IServiceDefinitionFactory'),
    TemplateService: Symbol.for('ITemplateService')
};

export default TYPES;

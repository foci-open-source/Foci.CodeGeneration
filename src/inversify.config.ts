import { Container } from 'inversify';
import 'reflect-metadata';
import TYPES from './constants/types';
import { FileRepository } from './orbital-cli.infrastructure/repositories/file.repository';
import { CreateServiceDefinitionRepository } from './orbital-cli.infrastructure/repositories/create-service-definition.repository';
import { IFileRepository } from './orbital-cli.infrastructure/repositories/interfaces/file.repository.interface';
import { ICreateServiceDefinitionRepository } from './orbital-cli.domain/aggregates/create-service-definition.aggregate/create-service-definition.repository.interface';
import { ITemplateService } from './orbital-cli.domain/aggregates/create-service-definition.aggregate/services/interfaces/template.service.interface';
import { TemplateService } from './orbital-cli.domain/aggregates/create-service-definition.aggregate/services/template.service';
import { IServiceDefinitionFactory } from './orbital-cli.domain/aggregates/create-service-definition.aggregate/factories/interfaces/service-definition.factory.interface';
import { ServiceDefinitionFactory } from './orbital-cli.domain/aggregates/create-service-definition.aggregate/factories/service-definition.factory';
import { BuildServiceDefinitionRepository } from './orbital-cli.infrastructure/repositories/build-service-definition.repository';
import { IBuildServiceDefinitionRepository } from './orbital-cli.domain/aggregates/build-service-definition.aggregate/build-service-definition.repository.interface';
import { IBuildServiceDefinitionParser } from './orbital-cli.infrastructure/parser/interfaces/build-service-definition.parser.interface';
import { BuildServiceDefinitionParser } from './orbital-cli.infrastructure/parser/build-service-definition.parser';
import { IAmqpRepository } from './orbital-cli.infrastructure/repositories/interfaces/amqp.repository.interface';
import { AmqpRepository } from './orbital-cli.infrastructure/repositories/amqp.repository';
import { ICommandHandler } from './oclif/handlers/interfaces/command.handler.interface';
import { CreateServiceHandler } from './oclif/handlers/create-service.handler';
import { CreateServiceDTO } from './oclif/data-transfers/create-service.dto';
import { CreateOperationDTO } from './oclif/data-transfers/create-operation.dto';
import { CreateOperationHandler } from './oclif/handlers/create-operation.handler';
import { IConsulRepository } from './orbital-cli.infrastructure/repositories/interfaces/consul.repository.interface';
import { ConsulRepository } from './orbital-cli.infrastructure/repositories/consul.repository';
import { IPublishServiceDefinitionRepository } from './orbital-cli.domain/aggregates/publish-service-definition.aggregate/publish-service-definition.repository.interface';
import { PublishServiceDefinitionRepository } from './orbital-cli.infrastructure/repositories/publish-service-definition.repository';


const cliContainer = new Container();

cliContainer.bind<IFileRepository>(TYPES.FileRepository).to(FileRepository);

cliContainer.bind<ITemplateService>(TYPES.TemplateService).to(TemplateService);

cliContainer
    .bind<ICreateServiceDefinitionRepository>(
        TYPES.CreateServiceDefinitionRepository
    )
    .to(CreateServiceDefinitionRepository);

cliContainer
    .bind<IBuildServiceDefinitionRepository>(
        TYPES.BuildServiceDefinitionRepository
    )
    .to(BuildServiceDefinitionRepository);

cliContainer
    .bind<IPublishServiceDefinitionRepository>(
        TYPES.PublishServiceDefinitionRepository
    )
    .to(PublishServiceDefinitionRepository);

cliContainer
    .bind<IBuildServiceDefinitionParser>(TYPES.BuildServiceDefinitionParser)
    .to(BuildServiceDefinitionParser);

cliContainer.bind<IAmqpRepository>(TYPES.AmqpRepository).to(AmqpRepository);

cliContainer.bind<IConsulRepository>(TYPES.ConsulRepository).to(ConsulRepository);

cliContainer
    .bind<ICommandHandler<CreateServiceDTO>>(TYPES.CreateServiceHandler)
    .to(CreateServiceHandler);

cliContainer
    .bind<ICommandHandler<CreateOperationDTO>>(TYPES.CreateOperationHandler)
    .to(CreateOperationHandler);

cliContainer
    .bind<IServiceDefinitionFactory>(TYPES.ServiceDefinitionFactory)
    .to(ServiceDefinitionFactory)
    .inSingletonScope();

export { cliContainer };

export interface IFileRepository {
    /**
     * This function is used to check if a folder or file already exists
     * @param {string} path The path of the file or folder
     * @returns {boolean} The boolean value to indicate exist or not
     */
    checkExist(path: string): boolean;

    /**
     * This function is used to create a folder. Using a path
     * parameter to designate where the folder needs to be created.
     * We check if the directory exists already or if its valid, then
     * we create the necessary folder.
     * @param {string} path string representation of a directory path
     * @returns {boolean} The boolean value to indicate create folder
     * success or not
     */
    createFolder(path: string): boolean;

    /**
     * This function takes in a path and content parameter, using
     * the path for the directory the file will be created in and
     * the content will be provided for the file as the other parameter
     * to fill the newly created file.
     * @param {string} path string representation of directory path
     * @param {string} content data to be written to new file
     * @returns {boolean} The boolean value to indicate create file
     * success or not
     */
    createFile(path: string, content: string): boolean;

    /**
     * This function takes in a path and content parameter, using
     * the path for the directory the file will be replaced in and
     * the content will be over written for the file as the other parameter
     * to fill the exist file.
     */
    replaceFile(path: string, content: string): boolean;

    /**
     * This function takes in a parameter of string
     * representing the path of the file needing to be read.
     * The function reads from the path and error checks the
     * reading before returning the content of the read.
     * @param {string} path string representation of directory path
     * @returns {string} string representation of data content been fetched from file
     */
    readFile(path: string): string;

    /**
     * This function will take in the path of the folder then scan and
     * returns the array of folders inside the path provided.
     * @param {string} path The path that take in
     * @returns {Array<string>} the array of folders inside the path provided
     */
    readDirectory(path: string): Array<string>;

    /**
     * The function will take in a path string and return a flag to indicate
     * the provided is a directory or not
     * @param {string} path The path provided
     * @returns {boolean} Flag to indicate the path provided is a directory or not
     */
    isDirectory(path: string): boolean;
}

import { OrbitalConfiguration } from '../../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/orbital-configuration';

export interface IConsulRepository {
    /**
     * function to publish the service definition to Consul.
     * @param serviceDefinition string representation of the Service Definition
     */
    publishServiceDefinition(
        serviceDefinition: string,
        serviceName: string
    ): Promise<boolean>;

    createConsulConnection(orbitalConfiguration: OrbitalConfiguration);

    getServiceDefinition(serviceName: string);
}

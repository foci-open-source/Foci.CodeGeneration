import { Rabbitmq } from '../../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/rabbitmq-configuration';
import { UpdateService } from '../../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/updateservice';

export interface IAmqpRepository {
    /**
     * This function will publish to the Agent's queue to notify that a service was updated or created and the Agent should know about it.
     * @param rabbitmq RabbitMQ's configuration to establish a connection.
     * @param serviceName The object representation of the name of the service to be pushed to the Agent's queue
     */
    publishToAgent(rabbitmq: Rabbitmq, serviceName: UpdateService);

}

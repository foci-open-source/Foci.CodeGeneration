import { IConsulRepository } from './interfaces/consul.repository.interface';
import Consul = require('consul');
import { ConsulOptions } from 'consul';
import { OrbitalConfiguration } from '../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/orbital-configuration';
import { injectable } from 'inversify';

@injectable()
export class ConsulRepository implements IConsulRepository {
    private _consul: Consul.Consul;
    private _consulOptions: ConsulOptions;

    public createConsulConnection(orbitalConfiguration: OrbitalConfiguration) {
        this._consulOptions = {
            host: orbitalConfiguration.consul.address,
            port: orbitalConfiguration.consul.port,
            promisify: true,
            secure: false,
        };
        this._consul = new Consul(this._consulOptions);
    }

    /**
     * @inheritdoc
     */
    public async publishServiceDefinition(
        serviceDefinition: string,
        serviceName: string
    ): Promise<boolean> {
        console.log('Publishing to Consul');
        let success = await this.publishKVService(
            serviceDefinition,
            serviceName
        );
        return success;
    }

    public async getServiceDefinition(
        serviceName: string
    ): Promise<string> {

        let serviceKey = 'orbital.' + serviceName + '.definition';

        return await this._consul.kv.get<string>(serviceKey);
    }

    /**
     * Publishes the service definition as value and the service name as key.
     * @param serviceDefinition string representation of the service definition
     * @param serviceName string representation of the service name
     */
    private async publishKVService(
        serviceDefinition: string,
        serviceName: string
    ): Promise<boolean> {
        let serviceKey = 'orbital.' + serviceName + '.definition';
        let options = {
            key: serviceKey,
            value: serviceDefinition
        };
        return await this._consul.kv
            .set(options)
            .then(value => {
                return true;
            })
            .catch(err => {
                console.log(
                    'There was an error publishing to Consul: ' + err.message
                );
                return false;
            });
    }
}

import { inject, injectable } from 'inversify';
import * as path from 'path';
import TYPES from '../../constants/types';
import { IFileRepository } from './interfaces/file.repository.interface';
import { ServiceDefinition } from '../../orbital-cli.domain/aggregates/create-service-definition.aggregate/service-definition';
import ARTIFACTS from '../../constants/artifacts';
import { Operation } from '../../orbital-cli.domain/aggregates/create-service-definition.aggregate/operation';
import { ICreateServiceDefinitionRepository } from '../../orbital-cli.domain/aggregates/create-service-definition.aggregate/create-service-definition.repository.interface';
import { ServiceNotExistException } from '../../orbital-cli.domain/exceptions/service-not-exist.exception';
import { OperationAlreadyExistException } from '../../orbital-cli.domain/exceptions/operation-already-exist.exception';

/**
 * The class is the connection between domain layer and infrastructure layer which expose
 * the service definition domain to infrastructure.
 * @implements {ICreateServiceDefinitionRepository}
 */
@injectable()
export class CreateServiceDefinitionRepository
    implements ICreateServiceDefinitionRepository {
    constructor(
        @inject(TYPES.FileRepository) private fileRepository: IFileRepository
    ) {}

    /**
     *  @inheritdoc
     */
    public getServiceDefinition(): ServiceDefinition {
        let orbitalFilePath = path.join(ARTIFACTS.OrbitalFile);

        if (!this.fileRepository.checkExist(orbitalFilePath)) {
            throw new ServiceNotExistException(
                `Cannot create the operation without the service, please create a service first.`
            );
        }
        let orbital: string = this.fileRepository.readFile(orbitalFilePath);
        let serviceName: string = JSON.parse(orbital).serviceName;

        return new ServiceDefinition(serviceName);
    }

    /**
     *  @inheritdoc
     */
    public createServiceDefinition(
        serviceDefinition: ServiceDefinition
    ): ServiceDefinition {
        if (this.fileRepository.checkExist(serviceDefinition.serviceName)) {
            console.log(serviceDefinition.serviceName + ' already exists.');
        } else {
            let serviceFolderPath: string = serviceDefinition.serviceName;
            this.fileRepository.createFolder(serviceFolderPath);

            let orbitalFilePath = path.join(
                serviceFolderPath,
                ARTIFACTS.OrbitalFile
            );

            this.fileRepository.createFile(
                orbitalFilePath,
                serviceDefinition.orbitalFile
            );
            console.log(
                `Service: ${
                    serviceDefinition.serviceName
                } was created successfully`
            );
        }

        serviceDefinition.operations.forEach((operation: Operation) => {
            let operationFolderPath: string = path.join(
                serviceDefinition.serviceName,
                operation.operationName
            );
            this.createOperation(operation, operationFolderPath);
            console.log(
                `Operation: ${
                    operation.operationName
                } was created successfully and added to ${
                    serviceDefinition.serviceName
                }`
            );
        });

        return serviceDefinition;
    }

    /**
     *  @inheritdoc
     */
    public addOperationToExistServiceDefinition(
        serviceDefinition: ServiceDefinition,
        operationName: string
    ): ServiceDefinition {
        const operation: Operation = serviceDefinition.getOperation(
            operationName
        );

        this.createOperation(operation, operationName);
        console.log(
            `Operation: ${operationName} was created successfully and added to ${
                serviceDefinition.serviceName
            }`
        );
        return serviceDefinition;
    }

    /**
     * The function to create the operation with files and folders
     * @param {Operation} operation The operation will be persist with files and folders
     * @param {string} operationFolderPath The path of the operation folder
     */
    private createOperation(operation: Operation, operationFolderPath: string) {
        if (this.fileRepository.checkExist(operationFolderPath)) {
            throw new OperationAlreadyExistException(
                `Operation ${operation.operationName} already exists`
            );
        }

        this.fileRepository.createFolder(operationFolderPath);

        let schemasFolder: string = path.join(
            operationFolderPath,
            ARTIFACTS.SchemasFolder
        );

        this.fileRepository.createFolder(schemasFolder);

        let requestSchemaPath: string = path.join(
            schemasFolder,
            ARTIFACTS.RequestSchemaFile
        );
        this.fileRepository.createFile(
            requestSchemaPath,
            operation.schemas.request
        );

        let responseSchemaPath: string = path.join(
            schemasFolder,
            ARTIFACTS.ResponseSchemaFile
        );
        this.fileRepository.createFile(
            responseSchemaPath,
            operation.schemas.response
        );

        let translationFolderPath: string = path.join(
            operationFolderPath,
            ARTIFACTS.TranslationsFolder
        );

        this.fileRepository.createFolder(translationFolderPath);

        let translationFilePath: string = path.join(
            translationFolderPath,
            ARTIFACTS.TranslationFile
        );

        this.fileRepository.createFile(
            translationFilePath,
            operation.translation
        );

        let adapterConfigurationsFolderPath: string = path.join(
            operationFolderPath,
            ARTIFACTS.AdapterConfigurationsFolder
        );

        this.fileRepository.createFolder(adapterConfigurationsFolderPath);

        let configurationFilePath: string = path.join(
            adapterConfigurationsFolderPath,
            ARTIFACTS.AdapterConfigFile
        );

        this.fileRepository.createFile(
            configurationFilePath,
            operation.adapter.adapter
        );

        let operationConfigurationFilePath: string = path.join(
            operationFolderPath,
            ARTIFACTS.OperationConfigurationFile
        );

        this.fileRepository.createFile(
            operationConfigurationFilePath,
            operation.operationConfiguration
        );
    }
}

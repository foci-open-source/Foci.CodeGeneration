import { IBuildServiceDefinitionRepository } from '../../orbital-cli.domain/aggregates/build-service-definition.aggregate/build-service-definition.repository.interface';
import { ServiceDefinition } from '../../orbital-cli.domain/aggregates/build-service-definition.aggregate/service-definition';
import { inject, injectable } from 'inversify';
import TYPES from '../../constants/types';
import { IBuildServiceDefinitionParser } from '../parser/interfaces/build-service-definition.parser.interface';
import { BuildParseException } from '../../orbital-cli.domain/exceptions/build-parse-exception';

@injectable()
export class BuildServiceDefinitionRepository
    implements IBuildServiceDefinitionRepository {
    constructor(
        @inject(TYPES.BuildServiceDefinitionParser)
        private buildServiceDefinitionParser: IBuildServiceDefinitionParser
    ) {}

    /**
     *  @inheritdoc
     */
    public BuildServiceDefinition(): ServiceDefinition {
        try {
            return this.buildServiceDefinitionParser.processServiceDefinition();
        } catch (error) {
            if (error instanceof BuildParseException) {
                console.error(error.message);
            } else {
                throw error;
            }
        }
    }
}

import { inject, injectable } from 'inversify';
import { IConsulRepository } from './interfaces/consul.repository.interface';
import { IAmqpRepository } from './interfaces/amqp.repository.interface';
import * as path from 'path';
import ARTIFACTS from '../../constants/artifacts';
import { IFileRepository } from './interfaces/file.repository.interface';
import { OrbitalConfiguration } from '../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/orbital-configuration';
import { FileDoesntExistException } from '../../orbital-cli.domain/exceptions/file-doesnt-exist.exception';
import { UpdateService } from '../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/updateservice';
import { IPublishServiceDefinitionRepository } from '../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/publish-service-definition.repository.interface';
import { Rabbitmq } from '../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/rabbitmq-configuration';
import { ConsulConfiguration } from '../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/consul-configuration';
import TYPES from '../../constants/types';

@injectable()
export class PublishServiceDefinitionRepository
    implements IPublishServiceDefinitionRepository {
    private _orbitalConfiguration: OrbitalConfiguration;
    constructor(
        @inject(TYPES.ConsulRepository)
        private consulRepository: IConsulRepository,
        @inject(TYPES.AmqpRepository) private amqpRepository: IAmqpRepository,
        @inject(TYPES.FileRepository) private fileRepository: IFileRepository
    ) {}

    public async publishServiceDefinition() {
        this._orbitalConfiguration = this.getOrbitalConfiguration();
        this.consulRepository.createConsulConnection(
            this._orbitalConfiguration
        );
        let serviceDefinition = this.getServiceDefinition();

        let success: boolean = await this.consulRepository.publishServiceDefinition(
            serviceDefinition,
            this._orbitalConfiguration.serviceName
        );

        if (success) {
            let updateService = new UpdateService(
                this._orbitalConfiguration.serviceName
            );
            this.amqpRepository.publishToAgent(
                this._orbitalConfiguration.rabbitmq,
                updateService
            );
        } else {
            console.log('Failed to publish. Please try again.');
        }
    }

    public async checkServiceDefinition(): Promise<boolean> {
        let serviceDefinition = this.getServiceDefinition();

        let _orbitalConfiguration = this.getOrbitalConfiguration();
        this.consulRepository.createConsulConnection(_orbitalConfiguration);
        let consulServiceDefinition = await this.consulRepository.getServiceDefinition(
            _orbitalConfiguration.serviceName
        );

        if (consulServiceDefinition === undefined) {
            return false;
        }

        return serviceDefinition === consulServiceDefinition.Value;
    }

    private getOrbitalConfiguration(): OrbitalConfiguration {
        let orbitalFilePath = path.join(ARTIFACTS.OrbitalFile);
        if (!this.fileRepository.checkExist(orbitalFilePath)) {
            throw new FileDoesntExistException(
                orbitalFilePath + ' does not exist.'
            );
        }
        let jsonParsed = JSON.parse(
            this.fileRepository.readFile(orbitalFilePath)
        );
        let rabbitmq = new Rabbitmq(
            jsonParsed.rabbit.address,
            jsonParsed.rabbit.port,
            jsonParsed.rabbit.username,
            jsonParsed.rabbit.password
        );
        let consul = new ConsulConfiguration(
            jsonParsed.consul.address,
            jsonParsed.consul.port
        );
        return new OrbitalConfiguration(
            rabbitmq,
            consul,
            jsonParsed.serviceName
        );
    }

    private getServiceDefinition(): string {
        let binfolder = path.join(ARTIFACTS.BinFolder);
        let serviceDefinitionFilePath = path.join(
            binfolder,
            ARTIFACTS.ServiceDefinitionFile
        );
        if (!this.fileRepository.checkExist(serviceDefinitionFilePath)) {
            throw new FileDoesntExistException(
                'Service Definition does not exist.'
            );
        }
        return this.fileRepository.readFile(serviceDefinitionFilePath);
    }
}

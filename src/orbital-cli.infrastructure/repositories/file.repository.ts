import * as fs from 'fs';
import { injectable } from 'inversify';

import { IFileRepository } from './interfaces/file.repository.interface';

/**
 * This class is responsible for managing the file and folder
 * creation in the Orbital CLI application.
 */
@injectable()
export class FileRepository implements IFileRepository {
    /**
     *  @inheritdoc
     */
    public checkExist(path: string): boolean {
        return fs.existsSync(path);
    }

    /**
     *  @inheritdoc
     */
    public createFolder(path: string): boolean {
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
            return true;
        }
        return false;
    }

    /**
     *  @inheritdoc
     */
    public createFile(path: string, content: string): boolean {
        if (!path || !content) {
            console.log(
                'Path and/or content are invalid ' +
                    path +
                    'content: ' +
                    content
            );
            return false;
        }

        if (fs.existsSync(path)) {
            console.log('File already exists');
            return false;
        }

        fs.writeFileSync(path, content);
        return true;
    }

    /**
     *  @inheritdoc
     */
    public replaceFile(path: string, content: string): boolean {
        fs.writeFileSync(path, content);
        return true;
    }

    /**
     *  @inheritdoc
     */
    public readFile(path: string): string {
        let content;
        try {
            content = fs.readFileSync(path).toString();
        } catch (err) {
            console.error('Error while reading file ' + err);
        }
        if (content !== undefined) {
            return content;
        }
        console.error(`Error while reading file ${path}`);
        return '';
    }

    /**
     *  @inheritdoc
     */
    public readDirectory(path: string): Array<string> {
        return fs.readdirSync(path);
    }

    /**
     *  @inheritdoc
     */
    public isDirectory(path: string): boolean {
        return fs.lstatSync(path).isDirectory();
    }
}

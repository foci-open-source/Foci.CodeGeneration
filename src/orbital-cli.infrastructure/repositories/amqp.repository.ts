import { IAmqpRepository } from './interfaces/amqp.repository.interface';
import { injectable } from 'inversify';
import { Rabbitmq } from '../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/rabbitmq-configuration';
import { UpdateService } from '../../orbital-cli.domain/aggregates/publish-service-definition.aggregate/updateservice';
import * as amqp from 'amqplib';
import CONNECTIONS from '../../constants/connection-names';

@injectable()
export class AmqpRepository implements IAmqpRepository {
    public async publishToAgent(
        rabbitmq: Rabbitmq,
        serviceName: UpdateService
    ) {
        try {
            let connection = await amqp.connect({
                hostname: rabbitmq.address,
                password: rabbitmq.password,
                port: Number(rabbitmq.port),
                username: rabbitmq.username
            });
            let serviceNamejson = JSON.stringify(serviceName);
            let channel = await connection.createChannel();
            await channel.assertExchange(CONNECTIONS.agentqueue, 'topic', {
                durable: true
            });
            console.log('Publishing to RabbitMQ');
            await channel.publish(
                CONNECTIONS.agentqueue,
                CONNECTIONS.agentqueue,
                new Buffer(serviceNamejson),
                { type: CONNECTIONS.typeAgent, persistent: true }
            );
            await channel.close();
            await connection.close();
        } catch (error) {
            console.log('Unable to publish to RabbitMQ: ' + error);
        }
    }
}

import { ServiceDefinition } from '../../orbital-cli.domain/aggregates/build-service-definition.aggregate/service-definition';
import { inject, injectable } from 'inversify';
import TYPES from '../../constants/types';
import { IFileRepository } from '../repositories/interfaces/file.repository.interface';
import ARTIFACTS from '../../constants/artifacts';
import { Operation } from '../../orbital-cli.domain/aggregates/build-service-definition.aggregate/operation';
import * as path from 'path';
import { minify } from 'uglify-js';
import { IBuildServiceDefinitionParser } from './interfaces/build-service-definition.parser.interface';
import { BuildParseException } from '../../orbital-cli.domain/exceptions/build-parse-exception';

@injectable()
export class BuildServiceDefinitionParser
    implements IBuildServiceDefinitionParser {
    constructor(
        @inject(TYPES.FileRepository) private fileRepository: IFileRepository
    ) {}

    public processServiceDefinition(): ServiceDefinition {
        let orbitalFilePath = path.join(ARTIFACTS.OrbitalFile);

        let service = this.parseJSONFile(orbitalFilePath);
        let serviceName: string = service.serviceName;
        let serviceDefinition: ServiceDefinition = new ServiceDefinition(
            serviceName
        );

        let binFolderPath: string = path.join(ARTIFACTS.BinFolder);

        // create the bin folder inside the service folder
        this.fileRepository
            .readDirectory('.')
            .filter(
                content =>
                    this.fileRepository.isDirectory(content) &&
                    content !== ARTIFACTS.BinFolder
            )
            .forEach(content => {
                let operation = serviceDefinition.addOperation(content);
                this.parseOperation(operation);
            });

        this.fileRepository.createFolder(binFolderPath);

        let serviceDefinitionFilePath = path.join(
            binFolderPath,
            ARTIFACTS.ServiceDefinitionFile
        );

        let serviceDefinitionJson: string = JSON.stringify(serviceDefinition);

        if (this.fileRepository.checkExist(serviceDefinitionFilePath)) {
            this.fileRepository.replaceFile(
                serviceDefinitionFilePath,
                serviceDefinitionJson
            );
        } else {
            this.fileRepository.createFile(
                serviceDefinitionFilePath,
                serviceDefinitionJson
            );
        }

        console.log(`${serviceDefinition.serviceName} build successfully`);

        return serviceDefinition;
    }

    private parseOperation(operation: Operation): Operation {
        let operationFolderPath = path.join(operation.operationName);

        this.fileRepository.readDirectory(operationFolderPath).forEach(path => {
            switch (path) {
                case ARTIFACTS.AdapterConfigurationsFolder:
                    this.parseAdapter(operation);
                    break;
                case ARTIFACTS.TranslationsFolder:
                    this.processTranslation(operation);
                    break;
                case ARTIFACTS.SchemasFolder:
                    this.parseSchemas(operation);
                    break;
                case ARTIFACTS.OperationConfigurationFile:
                    this.parseOperationConfig(operation);
                    break;
            }
        });

        return operation;
    }

    private parseAdapter(operation: Operation): Operation {
        let adapterConfigPath = path.join(
            operation.operationName,
            ARTIFACTS.AdapterConfigurationsFolder,
            ARTIFACTS.AdapterConfigFile
        );

        let adapterConfig = this.parseJSONFile(adapterConfigPath);

        if (!adapterConfig.type || !adapterConfig.configuration) {
            throw new BuildParseException(
                'Adapter config is invalid: please specify adapter type or adapter configuration'
            );
        }

        let adapterType = adapterConfig.type;

        let configuration = JSON.stringify(adapterConfig.configuration);

        operation.addAdapter(adapterType, configuration);

        return operation;
    }

    private parseSchemas(operation: Operation): Operation {
        let requestSchemaFilePath: string = path.join(
            operation.operationName,
            ARTIFACTS.SchemasFolder,
            ARTIFACTS.RequestSchemaFile
        );

        let responseSchemaFilePath: string = path.join(
            operation.operationName,
            ARTIFACTS.SchemasFolder,
            ARTIFACTS.ResponseSchemaFile
        );

        let request: string = JSON.stringify(
            this.parseJSONFile(requestSchemaFilePath)
        );

        let response: string = JSON.stringify(
            this.parseJSONFile(responseSchemaFilePath)
        );

        operation.addSchemas(request, response);

        return operation;
    }

    private processTranslation(operation: Operation): Operation {
        let translationFolderPath: string = path.join(
            operation.operationName,
            ARTIFACTS.TranslationsFolder
        );
        let code: any = {};
        this.fileRepository
            .readDirectory(translationFolderPath)
            .forEach(file => {
                let translationFilePath: string = path.join(
                    operation.operationName,
                    ARTIFACTS.TranslationsFolder,
                    file
                );
                code[file] = this.fileRepository.readFile(translationFilePath);
            });

        let result: string = minify(code).code;
        operation.addTranslation(result);

        return operation;
    }

    private parseOperationConfig(operation: Operation): Operation {
        let operationConfigFolderPath: string = path.join(
            operation.operationName,
            ARTIFACTS.OperationConfigurationFile
        );
        let operationConfig = JSON.parse(
            this.fileRepository.readFile(operationConfigFolderPath)
        );

        operation
            .addOperationSync(operationConfig.sync)
            .addHttpConfiguration(
                operationConfig.http.enabled,
                operationConfig.http.verb,
                operationConfig.http.verbOnly
            )
            .addValidation(
                operationConfig.validate.request,
                operationConfig.validate.response
            );

        if (operationConfig.sync === undefined) {
            throw new BuildParseException(
                'Operation config is invalid: please specify sync type'
            );
        }
        return operation;
    }

    private parseJSONFile(path: string): any {
        try {
            return JSON.parse(this.fileRepository.readFile(path));
        } catch (error) {
            throw new BuildParseException(error.message + ` ${path}`);
        }
    }
}

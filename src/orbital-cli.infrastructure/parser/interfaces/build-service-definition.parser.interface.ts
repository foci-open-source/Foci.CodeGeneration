import { ServiceDefinition } from '../../../orbital-cli.domain/aggregates/build-service-definition.aggregate/service-definition';

export interface IBuildServiceDefinitionParser {
    processServiceDefinition(): ServiceDefinition;
}
